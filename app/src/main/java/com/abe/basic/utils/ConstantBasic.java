package com.abe.basic.utils;

public class ConstantBasic {
    //SP-key
    public static final String STATUS_LOGIN = "STATUS_LOGIN";//登录状态
    public static final String ENTITY_USER = "ENTITY_USER";//用户信息

    //广播
    public static final String ABE_BASIC_AUTO_LOGIN = "com.abe.basic.autologin";
    public static final String ABE_BASIC_USERENTITY_UPDATE = "com.abe.basic.userinfo.update";
}
