package com.abe.basic.utils;


public class ConstantAPI {
    public static final String WApiBase = "http://www.wanandroid.com/";

    public static final String WApiLogin = "user/login";
    public static final String WApiRegister = "user/register";

    public static final String WApiBanner = "banner/json";//banner
    public static final String WApiArticle = "article/list/{page}/json";//获取文章列表
    public static final String WApiFriendLink = "friend/json";//常用链接

    public static final String WApiTree = "tree/json";//体系数据
    public static final String WApiTreeArticle = "article/list/{page}/json";//体系下文章

    //收藏文章
    public static final String WApiCollectArticleList = "lg/collect/list/{page}/json";//收藏站内文章列表
    public static final String WApiCollectInArticle = "lg/collect/{id}/json";//收藏站内文章
    public static final String WApiCollectOutArticle = "lg/collect/add/json";//收藏站外文章
    public static final String WApiUnCollectArticleIn = "lg/uncollect_originId/{id}/json";//取消收藏（列表）
    public static final String WApiUnCollectArticle = "lg/uncollect/{id}/json";//取消收藏(我的)
    //收藏网站
    public static final String WApiCollectWebList = "lg/collect/usertools/json";//列表
    public static final String WApiCollectAddWeb = "lg/collect/addtool/json";//添加
    public static final String WApiCollectUpdateWeb = "lg/collect/updatetool/json";//编辑
    public static final String WApiUnCollectWeb = "lg/collect/deletetool/json";//删除
}
