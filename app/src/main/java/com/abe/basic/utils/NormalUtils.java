package com.abe.basic.utils;

import android.content.Context;
import android.graphics.Color;

import com.abe.basic.model.http.HttpUserEntity;
import com.abe.gcore.utils.CoreNormalUtils;
import com.abe.gcore.utils.SPUtils;
import com.google.gson.Gson;
import com.maning.mndialoglibrary.MProgressDialog;

public class NormalUtils {

    public static HttpUserEntity getUser(SPUtils spUtils) {
        return new Gson().fromJson(spUtils.getString(ConstantBasic.ENTITY_USER), HttpUserEntity.class);
    }

    public static MProgressDialog creatDialog(Context context) {
        return new MProgressDialog.Builder(context)
                //点击外部是否可以取消
                .isCanceledOnTouchOutside(true)
                //全屏背景窗体的颜色
                .setBackgroundWindowColor(CoreNormalUtils.getColor(com.abe.gcore.R.color.txt_9_t))
                //View背景的颜色
                .setBackgroundViewColor(CoreNormalUtils.getColor(com.abe.gcore.R.color.txt_3_t))
                //View背景的圆角
                .setCornerRadius(20)
                //View 边框的颜色
                .setStrokeColor(CoreNormalUtils.getColor(com.abe.gcore.R.color.colorAccent))
                //View 边框的宽度
                .setStrokeWidth(2)
                //Progress 颜色
                .setProgressColor(CoreNormalUtils.getColor(com.abe.gcore.R.color.white))
                //Progress 宽度
                .setProgressWidth(3)
                //Progress 内圈颜色
                .setProgressRimColor(Color.TRANSPARENT)
                //Progress 内圈宽度
                .setProgressRimWidth(0)
                //文字的颜色
                .setTextColor(CoreNormalUtils.getColor(com.abe.gcore.R.color.white))
                //取消的监听
                .setOnDialogDismissListener(new MProgressDialog.OnDialogDismissListener() {
                    @Override
                    public void dismiss() {
                    }
                })
                .build();
    }
}
