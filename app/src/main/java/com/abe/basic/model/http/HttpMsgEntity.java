package com.abe.basic.model.http;

import java.io.Serializable;


public class HttpMsgEntity implements Serializable {
    private int errorCode;

    private String errorMsg;

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public int getErrorCode() {
        return this.errorCode;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getErrorMsg() {
        return this.errorMsg;
    }
}
