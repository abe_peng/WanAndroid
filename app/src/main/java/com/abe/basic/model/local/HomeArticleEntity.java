package com.abe.basic.model.local;


import com.abe.basic.model.http.HttpArticleEntity;

import java.io.Serializable;

public class HomeArticleEntity implements Serializable {
    private HttpArticleEntity articleEntity;
    private Boolean isCollect;

    public HttpArticleEntity getArticleEntity() {
        return articleEntity;
    }

    public void setArticleEntity(HttpArticleEntity articleEntity) {
        this.articleEntity = articleEntity;
    }

    public Boolean getCollect() {
        return isCollect;
    }

    public void setCollect(Boolean collect) {
        isCollect = collect;
    }
}
