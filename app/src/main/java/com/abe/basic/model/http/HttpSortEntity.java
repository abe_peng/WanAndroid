package com.abe.basic.model.http;


import java.io.Serializable;
import java.util.List;

public class HttpSortEntity implements Serializable {

    private List<HttpSortEntity> children;

    private int courseId;

    private int id;

    private String name;

    private int order;

    private int parentChapterId;

    private int visible;

    public void setChildren(List<HttpSortEntity> children) {
        this.children = children;
    }

    public List<HttpSortEntity> getChildren() {
        return this.children;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public int getCourseId() {
        return this.courseId;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public int getOrder() {
        return this.order;
    }

    public void setParentChapterId(int parentChapterId) {
        this.parentChapterId = parentChapterId;
    }

    public int getParentChapterId() {
        return this.parentChapterId;
    }

    public void setVisible(int visible) {
        this.visible = visible;
    }

    public int getVisible() {
        return this.visible;
    }
}
