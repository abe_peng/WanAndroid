package com.abe.basic.android.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.abe.basic.R;
import com.abe.basic.android.CommonWebActivity;
import com.abe.basic.android.common.CollectContract;
import com.abe.basic.android.common.presenter.CollectWebPresenter;
import com.abe.basic.model.http.HttpWebEntity;
import com.abe.basic.utils.ConstantBasic;
import com.abe.basic.utils.NormalUtils;
import com.abe.gcore.base.DaggerBaseFragment;
import com.abe.gcore.utils.CoreNormalUtils;
import com.abe.gcore.utils.SPUtils;
import com.maning.mndialoglibrary.MProgressDialog;
import com.yanzhenjie.recyclerview.swipe.widget.DefaultItemDecoration;
import com.zhy.adapter.recyclerview.CommonAdapter;
import com.zhy.adapter.recyclerview.MultiItemTypeAdapter;
import com.zhy.adapter.recyclerview.base.ViewHolder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class CollectWebFragment extends DaggerBaseFragment<CollectContract.CollectWebView, CollectWebPresenter> implements CollectContract.CollectWebView {

    @BindView(R.id.item_rv)
    RecyclerView itemRv;

    private CommonAdapter commonAdapter;
    private List<HttpWebEntity> webEntities;

    @Inject
    SPUtils spUtils;
    MProgressDialog mProgressDialog;

    @Override
    protected CollectWebPresenter createPresenter() {
        return new CollectWebPresenter();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_collect_web;
    }

    @Override
    protected void initDatas() {
        mProgressDialog = NormalUtils.creatDialog(getThisContext());
        webEntities = new ArrayList<>();
        commonAdapter = new CommonAdapter<HttpWebEntity>(getThisContext(), R.layout.item_list_collect_web, webEntities) {
            @Override
            protected void convert(ViewHolder holder, final HttpWebEntity httpWebEntity, int position) {
                holder.setText(R.id.item_name, httpWebEntity.getName());
                holder.getView(R.id.item_img_update).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean isLogin = spUtils.getBoolean(ConstantBasic.STATUS_LOGIN, false);
                        if (isLogin) {
                            createDialog(false, httpWebEntity);
                        }
                    }
                });
                holder.getView(R.id.item_img_delete).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean isLogin = spUtils.getBoolean(ConstantBasic.STATUS_LOGIN, false);
                        if (isLogin) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(getThisContext()).setTitle("提示").setMessage("删除收藏？").setNegativeButton("取消", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    mPresenter.unCollect(httpWebEntity.getId());
                                }
                            });
                            builder.show();
                        }
                    }
                });
            }
        };
        initRV();
        mPresenter.getCollectList();
        mProgressDialog.show();
    }

    private void initRV() {
        itemRv.setLayoutManager(new LinearLayoutManager(getContext()));
        itemRv.addItemDecoration(new DefaultItemDecoration(CoreNormalUtils.getColor(R.color.transparent)));
        itemRv.setAdapter(commonAdapter);
        commonAdapter.setOnItemClickListener(new MultiItemTypeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
                HttpWebEntity webEntity = webEntities.get(position);
                HashMap<String, Object> map = new HashMap<>();
                map.put("url", webEntity.getLink());
                map.put("name", webEntity.getName());
                goToActivity(CommonWebActivity.class, map);
            }

            @Override
            public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, int position) {
                return false;
            }
        });
    }

    @OnClick(R.id.item_add_collect)
    public void onViewClicked(View view) {
        HttpWebEntity httpWebEntity = new HttpWebEntity();
        httpWebEntity.setName("");
        httpWebEntity.setLink("");
        createDialog(true, httpWebEntity);
    }

    private void createDialog(final boolean isAdd, final HttpWebEntity httpWebEntity) {
        //弹窗提示修改
        View view = LayoutInflater.from(getThisContext()).inflate(R.layout.item_view_edit_web, null);
        final EditText name = view.findViewById(R.id.item_name);
        final EditText link = view.findViewById(R.id.item_link);
        name.setText(httpWebEntity.getName());
        link.setText(httpWebEntity.getLink());
        AlertDialog.Builder builder = new AlertDialog.Builder(getThisContext()).setTitle("收藏").setView(view).setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).setPositiveButton(isAdd ? "添加收藏" : "更新收藏", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String strName = CoreNormalUtils.getText(name);
                String strLink = CoreNormalUtils.getText(link);
                if (TextUtils.isEmpty(strName)) {
                    CoreNormalUtils.w("请填写名称");
                    return;
                }
                if (TextUtils.isEmpty(strLink)) {
                    CoreNormalUtils.w("请填写链接");
                    return;
                }
                dialog.dismiss();
                if (isAdd) {
                    mPresenter.collectAdd(strName, strLink);
                } else {
                    mPresenter.collectUpdate(httpWebEntity.getId(), strName, strLink);
                }
                mProgressDialog.show();
            }
        });
        builder.show();
    }

    @Override
    public void onGetWebList(List<HttpWebEntity> webEntities) {
        if (webEntities == null) return;
        this.webEntities.clear();
        this.webEntities.addAll(webEntities);
        refreshRV();
    }

    private void refreshRV() {
        itemRv.post(new Runnable() {
            @Override
            public void run() {
                commonAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onRequest(String type, boolean isSucc) {
        if (!isSucc) return;
        switch (type) {
            case "updateCollectWeb": {
                CoreNormalUtils.showShortToast("更新成功");
            }
            break;
            case "unCollectWeb": {
                CoreNormalUtils.showShortToast("删除成功");
            }
            break;
            case "addCollectWeb": {
                CoreNormalUtils.showShortToast("添加成功");
            }
            break;
        }
        mPresenter.getCollectList();
    }

    @Override
    public void onFinish(String type) {
        mProgressDialog.dismiss();
    }

    @Override
    public void showMessage(int Type, String message) {

    }

    @Override
    public void onFaild(String type) {
        switch (type) {
            case "updateCollectWeb": {
                CoreNormalUtils.showShortToast("更新失败");
            }
            break;
        }
    }

    public CollectWebFragment() {
    }

    public static CollectWebFragment newInstance() {
        return new CollectWebFragment();
    }


}
