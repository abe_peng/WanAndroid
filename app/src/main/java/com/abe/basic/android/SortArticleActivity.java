package com.abe.basic.android;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.abe.basic.R;
import com.abe.basic.android.fragment.SortArticleFragment;
import com.abe.basic.model.http.HttpSortEntity;
import com.abe.basic.utils.ConstantBasic;
import com.abe.basic.widget.TabMenu;
import com.abe.gcore.base.DaggerBaseFActivity;
import com.abe.gcore.base.mvp.IBasePresenter;
import com.abe.gcore.utils.CoreNormalUtils;
import com.abe.gcore.utils.SPUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class SortArticleActivity extends DaggerBaseFActivity {
    @BindView(R.id.item_menu_layout)
    LinearLayout itemMenuLayout;
    @BindView(R.id.tabs)
    HorizontalScrollView tabs;
    @BindView(R.id.item_viewpager)
    ViewPager itemViewpager;
    @BindView(R.id.item_title)
    TextView itemTitle;

    //菜单
    private ArrayList<TabMenu> layViews;
    private HttpSortEntity sortEntity;

    private List<SortArticleFragment> fragments;

    @Inject
    SPUtils spUtils;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_sort_article;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        sortEntity = (HttpSortEntity) getFilter().get("sort");
    }

    @Override
    protected IBasePresenter createPresenter() {
        return null;
    }

    @Override
    protected void initView() {
        itemTitle.setText(sortEntity.getName());
        initMenu();
        setAdapter();
        //默认第一组
        setSelector(0);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (spUtils.getBoolean(ConstantBasic.STATUS_LOGIN, false))
            CoreNormalUtils.sendBroadcast(ConstantBasic.ABE_BASIC_AUTO_LOGIN);
    }

    private void initMenu() {
        layViews = new ArrayList<>();
        fragments = new ArrayList<>();
        for (int i = 0; i < sortEntity.getChildren().size(); i++) {
            HttpSortEntity httpSortEntity = sortEntity.getChildren().get(i);
            addMenuView(i, httpSortEntity.getName());
            fragments.add(SortArticleFragment.newInstance(httpSortEntity.getId()));
        }
    }

    public void setAdapter() {
        itemViewpager.setAdapter(new myPagerView(getSupportFragmentManager(),
                fragments));
        itemViewpager.setOffscreenPageLimit(fragments.size());
        itemViewpager.clearAnimation();
        itemViewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int pos) {
                setSelector(pos);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int arg0) {

            }

        });
    }

    @OnClick(R.id.item_back)
    public void onViewClicked() {
        onBackPressed();
    }

    private class myPagerView extends FragmentPagerAdapter {

        private List<SortArticleFragment> pagerList;

        myPagerView(FragmentManager fm, List<SortArticleFragment> pagerItemList) {
            super(fm);
            this.pagerList = pagerItemList;
            if (pagerList == null) {
                pagerList = new ArrayList<>();
            }
        }

        @Override
        public int getCount() {
            return pagerList.size();
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment;
            if (position < pagerList.size())
                fragment = pagerList.get(position);
            else
                fragment = pagerList.get(0);
            return fragment;
        }
    }

    private void addMenuView(int i, String menuName) {
        TabMenu menu = new TabMenu(this);
        menu.setId(i);
        menu.setMenuName(menuName);
        menu.setMenuTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelector(v.getId());
            }
        });
        layViews.add(menu);
        itemMenuLayout.addView(menu);
    }

    /***
     * 选中效果
     */
    private void setSelector(int id) {
        if (layViews == null || layViews.size() == 0) {
            return;
        }
        for (int i = 0; i < layViews.size(); i++) {
            layViews.get(i).setSelector(false);
        }
        layViews.get(id).setSelector(true);
        int left = 0;
        for (int i = 0; i < id; i++) {
            left = left + layViews.get(i).getWidth();
        }
        if (id > 1) {
            tabs.smoothScrollTo(left - 90, 0);// 设置居中
        } else {
            tabs.smoothScrollTo(0, 0);
        }
        itemViewpager.setCurrentItem(id);
        fragments.get(id).fristLoadData();
    }
}
