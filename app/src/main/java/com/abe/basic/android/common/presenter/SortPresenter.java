package com.abe.basic.android.common.presenter;

import com.abe.basic.android.common.CommonContract;
import com.abe.basic.api.AppMainService;
import com.abe.basic.model.http.HttpArticleEntity;
import com.abe.basic.model.http.HttpPageEntity;
import com.abe.basic.model.http.HttpSortEntity;
import com.abe.gcore.base.BaseTResultEntity;
import com.abe.gcore.base.mvp.rxandroid.IRxBasePresenter;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class SortPresenter extends IRxBasePresenter<CommonContract.SortView> implements CommonContract.SortPresenter {
    @Override
    public void getArticleTree() {
        AppMainService.getSortService().getSortData().subscribeOn(Schedulers.io())
                .doOnSubscribe(DisposableConsumer())
                .map(new Function<BaseTResultEntity<List<HttpSortEntity>>, List<HttpSortEntity>>() {
                    @Override
                    public List<HttpSortEntity> apply(BaseTResultEntity<List<HttpSortEntity>> listBaseTResultEntity) throws Exception {
                        checkT(listBaseTResultEntity);
                        return listBaseTResultEntity.getData();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<HttpSortEntity>>() {
                    @Override
                    public void accept(@NonNull List<HttpSortEntity> entities) throws Exception {
                        checkData(entities, "getSortData", MainPresenter.class.getSimpleName());
                    }
                }, ThrowableConsumer(MainPresenter.class.getSimpleName()));
    }

    @Override
    public void getTreeArticles(int page, int cid) {
        AppMainService.getSortService().getSortArticle(page, cid).subscribeOn(Schedulers.io())
                .doOnSubscribe(DisposableConsumer())
                .map(new Function<BaseTResultEntity<HttpPageEntity<HttpArticleEntity>>, HttpPageEntity<HttpArticleEntity>>() {
                    @Override
                    public HttpPageEntity<HttpArticleEntity> apply(BaseTResultEntity<HttpPageEntity<HttpArticleEntity>> entity) throws Exception {
                        checkT(entity);
                        return entity.getData();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<HttpPageEntity<HttpArticleEntity>>() {
                    @Override
                    public void accept(@NonNull HttpPageEntity<HttpArticleEntity> entity) throws Exception {
                        checkData(entity, "getSortArticle", MainPresenter.class.getSimpleName());
                    }
                }, ThrowableConsumer(MainPresenter.class.getSimpleName()));
    }

    @Override
    protected void onCheckSuccess(Object obj, String method) {
        switch (method) {
            case "getSortData": {
                getView().onGetArticleTree((List<HttpSortEntity>) obj);
            }
            break;
            case "getSortArticle": {
                getView().onGetTreeArticles((HttpPageEntity<HttpArticleEntity>) obj);
            }
            break;
        }
    }
}
