package com.abe.basic.android.common.presenter;

import com.abe.basic.android.common.CollectContract;
import com.abe.basic.api.AppMainService;
import com.abe.basic.model.http.HttpCollectArticleEntity;
import com.abe.basic.model.http.HttpMsgEntity;
import com.abe.basic.model.http.HttpPageEntity;
import com.abe.gcore.base.BaseTResultEntity;
import com.abe.gcore.base.mvp.rxandroid.IRxBasePresenter;
import com.abe.gcore.utils.CoreNormalUtils;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class CollectArticlePresenter extends IRxBasePresenter<CollectContract.CollectArticleView> implements CollectContract.CollectArticlePresenter {

    @Override
    public void getCollectList(int page) {
        AppMainService.getCollectService()
                .getArticleList(page)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(DisposableConsumer())
                .map(new Function<BaseTResultEntity<HttpPageEntity<HttpCollectArticleEntity>>, HttpPageEntity<HttpCollectArticleEntity>>() {
                    @Override
                    public HttpPageEntity<HttpCollectArticleEntity> apply(BaseTResultEntity<HttpPageEntity<HttpCollectArticleEntity>> resultEntity) throws Exception {
                        checkT(resultEntity);
                        return resultEntity.getData();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<HttpPageEntity<HttpCollectArticleEntity>>() {
                    @Override
                    public void accept(@NonNull HttpPageEntity<HttpCollectArticleEntity> entities) throws Exception {
                        checkData(entities, "getArticleList", MainPresenter.class.getSimpleName());
                    }
                }, ThrowableConsumer(MainPresenter.class.getSimpleName()));
    }

    @Override
    public void collectIn(int id) {
        AppMainService.getCollectService()
                .collectInArticle(id)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(DisposableConsumer())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<HttpMsgEntity>() {
                    @Override
                    public void accept(@NonNull HttpMsgEntity entity) throws Exception {
                        checkData(entity, "collectInArticle", MainPresenter.class.getSimpleName());
                    }
                }, ThrowableConsumer(MainPresenter.class.getSimpleName()));
    }

    @Override
    public void collectOut(String title, String author, String link) {
        AppMainService.getCollectService()
                .collectOutArticle(title, author, link)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(DisposableConsumer())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<HttpMsgEntity>() {
                    @Override
                    public void accept(@NonNull HttpMsgEntity entity) throws Exception {
                        checkData(entity, "collectOutArticle", MainPresenter.class.getSimpleName());
                    }
                }, ThrowableConsumer(MainPresenter.class.getSimpleName()));
    }

    @Override
    public void unCollectIn(int id) {
        AppMainService.getCollectService()
                .unCollectArticleIn(id)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(DisposableConsumer())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<HttpMsgEntity>() {
                    @Override
                    public void accept(@NonNull HttpMsgEntity entity) throws Exception {
                        checkData(entity, "unCollectArticle", MainPresenter.class.getSimpleName());
                    }
                }, ThrowableConsumer(MainPresenter.class.getSimpleName()));
    }

    @Override
    public void unCollect(int id, int originId) {
        AppMainService.getCollectService()
                .unCollectArticle(id, originId)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(DisposableConsumer())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<HttpMsgEntity>() {
                    @Override
                    public void accept(@NonNull HttpMsgEntity entity) throws Exception {
                        checkData(entity, "unCollectArticle", MainPresenter.class.getSimpleName());
                    }
                }, ThrowableConsumer(MainPresenter.class.getSimpleName()));
    }

    @Override
    protected void onCheckSuccess(Object obj, String method) {
        switch (method) {
            case "getArticleList": {
                getView().onGetCollectArticles((HttpPageEntity<HttpCollectArticleEntity>) obj);
            }
            break;
            default: {
                HttpMsgEntity entity = (HttpMsgEntity) obj;
                if (entity.getErrorCode() < 0)
                    CoreNormalUtils.showShortToastR(entity.getErrorMsg());
                getView().onRequest(method, !(entity.getErrorCode() < 0));
            }
            break;
        }
    }
}
