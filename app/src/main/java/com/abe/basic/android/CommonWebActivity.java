package com.abe.basic.android;

import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.TextView;

import com.abe.basic.R;
import com.abe.basic.base.X5WebView;
import com.abe.gcore.base.DaggerBaseActivity;
import com.abe.gcore.base.mvp.IBasePresenter;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;

public class CommonWebActivity extends DaggerBaseActivity {
    @BindView(R.id.item_webview)
    X5WebView itemWebview;
    @BindView(R.id.item_title)
    TextView itemTitle;

    @Override
    protected IBasePresenter createPresenter() {
        return null;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_common_web;
    }

    @Override
    protected void init(Bundle savedInstanceState) {

    }

    @Override
    protected void initView() {
        HashMap<String, Object> map = getFilter();
        itemWebview.loadUrl(map.get("url").toString());
        itemTitle.setText(map.get("name").toString());
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (itemWebview != null && itemWebview.canGoBack()) {
                itemWebview.goBack();
                return true;
            } else
                return super.onKeyDown(keyCode, event);
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        if (itemWebview != null)
            itemWebview.destroy();
        super.onDestroy();
    }

    @OnClick(R.id.item_back)
    public void onViewClicked() {
        onBackPressed();
    }
}
