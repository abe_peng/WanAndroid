package com.abe.basic.android.common.presenter;

import com.abe.basic.android.common.CommonContract;
import com.abe.basic.api.AppMainService;
import com.abe.basic.model.http.HttpUserEntity;
import com.abe.gcore.base.BaseTResultEntity;
import com.abe.gcore.base.mvp.rxandroid.IRxBasePresenter;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class LoginRegisPresenter extends IRxBasePresenter<CommonContract.LoginRegisView> implements CommonContract.LoginRegisPresenter {

    @Override
    public void login(String username, String password) {
        AppMainService.getCommonService().login(username, password).subscribeOn(Schedulers.io())

                .doOnSubscribe(DisposableConsumer())
                .map(new Function<BaseTResultEntity<HttpUserEntity>, HttpUserEntity>() {
                    @Override
                    public HttpUserEntity apply(BaseTResultEntity<HttpUserEntity> resultEntity) throws Exception {
                        checkT(resultEntity);
                        return resultEntity.getData();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<HttpUserEntity>() {
                    @Override
                    public void accept(@NonNull HttpUserEntity entities) throws Exception {
                        checkData(entities, "login", MainPresenter.class.getSimpleName());
                    }
                }, ThrowableConsumer(MainPresenter.class.getSimpleName()));
    }

    @Override
    public void register(String username, String password, String passworda) {
        AppMainService.getCommonService().register(username, password, passworda).subscribeOn(Schedulers.io())

                .doOnSubscribe(DisposableConsumer())
                .map(new Function<BaseTResultEntity<HttpUserEntity>, HttpUserEntity>() {
                    @Override
                    public HttpUserEntity apply(BaseTResultEntity<HttpUserEntity> resultEntity) throws Exception {
                        checkT(resultEntity);
                        return resultEntity.getData();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<HttpUserEntity>() {
                    @Override
                    public void accept(@NonNull HttpUserEntity entities) throws Exception {
                        checkData(entities, "register", MainPresenter.class.getSimpleName());
                    }
                }, ThrowableConsumer(MainPresenter.class.getSimpleName()));
    }

    @Override
    protected void onCheckSuccess(Object obj, String method) {
        switch (method) {
            case "login": {
                getView().onResult(1, (HttpUserEntity) obj);
            }
            break;
            case "register": {
                getView().onResult(0, (HttpUserEntity) obj);
            }
            break;
        }
    }
}
