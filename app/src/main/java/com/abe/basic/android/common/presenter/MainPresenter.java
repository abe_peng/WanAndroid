package com.abe.basic.android.common.presenter;

import com.abe.basic.android.common.CommonContract;
import com.abe.basic.model.http.HttpArticleEntity;
import com.abe.gcore.base.BaseTResultEntity;
import com.abe.gcore.base.mvp.rxandroid.IRxBasePresenter;
import com.abe.basic.api.AppMainService;
import com.abe.basic.model.http.HttpPageEntity;
import com.abe.basic.model.http.HttpBannerEntity;
import com.abe.basic.model.http.HttpFriendLinkEntity;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class MainPresenter extends IRxBasePresenter<CommonContract.MainView> implements CommonContract.MainPresenter {

    @Override
    public void getBanner() {
        AppMainService.getMainService()
                .getBanner()
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(DisposableConsumer())
                .map(new Function<BaseTResultEntity<List<HttpBannerEntity>>, List<HttpBannerEntity>>() {
                    @Override
                    public List<HttpBannerEntity> apply(BaseTResultEntity<List<HttpBannerEntity>> listBaseTResultEntity) throws Exception {
                        checkT(listBaseTResultEntity);
                        return listBaseTResultEntity.getData();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<HttpBannerEntity>>() {
                    @Override
                    public void accept(@NonNull List<HttpBannerEntity> entities) throws Exception {
                        checkData(entities, "getBanner", MainPresenter.class.getSimpleName());
                    }
                }, ThrowableConsumer(MainPresenter.class.getSimpleName()));
    }

    @Override
    public void getArticleByPage(int page) {
        AppMainService.getMainService()
                .getArticleByPage(page)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(DisposableConsumer())
                .map(new Function<BaseTResultEntity<HttpPageEntity<HttpArticleEntity>>, HttpPageEntity<HttpArticleEntity>>() {
                    @Override
                    public HttpPageEntity<HttpArticleEntity> apply(BaseTResultEntity<HttpPageEntity<HttpArticleEntity>> entity) throws Exception {
                        checkT(entity);
                        return entity.getData();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<HttpPageEntity<HttpArticleEntity>>() {
                    @Override
                    public void accept(@NonNull HttpPageEntity<HttpArticleEntity> entity) throws Exception {
                        checkData(entity, "getArticleByPage", MainPresenter.class.getSimpleName());
                    }
                }, ThrowableConsumer(MainPresenter.class.getSimpleName()));
    }

    @Override
    public void getFriendLink() {
        AppMainService.getMainService()
                .getFriendLink()
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(DisposableConsumer())
                .map(new Function<BaseTResultEntity<List<HttpFriendLinkEntity>>, List<HttpFriendLinkEntity>>() {
                    @Override
                    public List<HttpFriendLinkEntity> apply(BaseTResultEntity<List<HttpFriendLinkEntity>> listBaseTResultEntity) throws Exception {
                        return listBaseTResultEntity.getData();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<HttpFriendLinkEntity>>() {
                    @Override
                    public void accept(@NonNull List<HttpFriendLinkEntity> entity) throws Exception {
                        checkData(entity, "getFriendLink", MainPresenter.class.getSimpleName());
                    }
                }, ThrowableConsumer(MainPresenter.class.getSimpleName()));
    }

    @Override
    protected void onCheckSuccess(Object obj, String method) {
        switch (method) {
            case "getBanner": {
                getView().onGetBanner((List<HttpBannerEntity>) obj);
            }
            break;
            case "getArticleByPage": {
                getView().onGetArticles((HttpPageEntity<HttpArticleEntity>) obj);
            }
            break;
            case "getFriendLink": {
                getView().onGetFriendLink((List<HttpFriendLinkEntity>) obj);
            }
            break;
        }
    }
}
