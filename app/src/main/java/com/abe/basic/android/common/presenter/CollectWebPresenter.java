package com.abe.basic.android.common.presenter;

import com.abe.basic.android.common.CollectContract;
import com.abe.basic.api.AppMainService;
import com.abe.basic.model.http.HttpMsgEntity;
import com.abe.basic.model.http.HttpWebEntity;
import com.abe.gcore.base.BaseTResultEntity;
import com.abe.gcore.base.mvp.rxandroid.IRxBasePresenter;
import com.abe.gcore.utils.CoreNormalUtils;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class CollectWebPresenter extends IRxBasePresenter<CollectContract.CollectWebView> implements CollectContract.CollectWebPresenter {

    @Override
    public void getCollectList() {
        AppMainService.getCollectService()
                .getWebList()
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(DisposableConsumer())
                .map(new Function<BaseTResultEntity<List<HttpWebEntity>>, List<HttpWebEntity>>() {
                    @Override
                    public List<HttpWebEntity> apply(BaseTResultEntity<List<HttpWebEntity>> resultEntity) throws Exception {
                        checkT(resultEntity);
                        return resultEntity.getData();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<HttpWebEntity>>() {
                    @Override
                    public void accept(@NonNull List<HttpWebEntity> entities) throws Exception {
                        checkData(entities, "getWebList", MainPresenter.class.getSimpleName());
                    }
                }, ThrowableConsumer("getWebList"));
    }

    @Override
    public void collectAdd(String name, String link) {
        AppMainService.getCollectService()
                .addCollectWeb(name, link)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(DisposableConsumer())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<HttpMsgEntity>() {
                    @Override
                    public void accept(@NonNull HttpMsgEntity entity) throws Exception {
                        checkData(entity, "addCollectWeb", MainPresenter.class.getSimpleName());
                    }
                }, ThrowableConsumer("addCollectWeb"));
    }

    @Override
    public void collectUpdate(int id, String name, String link) {
        AppMainService.getCollectService()
                .updateCollectWeb(id, name, link)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(DisposableConsumer())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<HttpMsgEntity>() {
                    @Override
                    public void accept(@NonNull HttpMsgEntity entity) throws Exception {
                        checkData(entity, "updateCollectWeb", MainPresenter.class.getSimpleName());
                    }
                }, ThrowableConsumer("updateCollectWeb"));
    }

    @Override
    public void unCollect(int id) {
        AppMainService.getCollectService()
                .unCollectWeb(id)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(DisposableConsumer())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<HttpMsgEntity>() {
                    @Override
                    public void accept(@NonNull HttpMsgEntity entity) throws Exception {
                        checkData(entity, "unCollectWeb", MainPresenter.class.getSimpleName());
                    }
                }, ThrowableConsumer("unCollectWeb"));
    }

    @Override
    protected void onCheckSuccess(Object obj, String method) {
        switch (method) {
            case "getWebList": {
                getView().onGetWebList((List<HttpWebEntity>) obj);
            }
            break;
            default: {
                HttpMsgEntity entity = (HttpMsgEntity) obj;
                if (entity.getErrorCode() < 0)
                    CoreNormalUtils.showShortToastR(entity.getErrorMsg());
                getView().onRequest(method, !(entity.getErrorCode() < 0));
            }
            break;
        }
    }
}
