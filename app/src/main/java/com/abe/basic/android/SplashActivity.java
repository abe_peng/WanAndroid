package com.abe.basic.android;

import android.os.Bundle;

import com.abe.basic.R;
import com.abe.basic.android.common.CommonContract;
import com.abe.basic.android.common.presenter.LoginRegisPresenter;
import com.abe.basic.model.http.HttpUserEntity;
import com.abe.basic.utils.ConstantBasic;
import com.abe.basic.utils.NormalUtils;
import com.abe.gcore.base.DaggerBaseActivity;
import com.abe.gcore.utils.SPUtils;
import com.google.gson.Gson;

import javax.inject.Inject;

public class SplashActivity extends DaggerBaseActivity<CommonContract.LoginRegisView, LoginRegisPresenter> implements CommonContract.LoginRegisView {

    @Inject
    SPUtils spUtils;

    @Override
    protected LoginRegisPresenter createPresenter() {
        return new LoginRegisPresenter();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_splash;
    }

    @Override
    protected void init(Bundle savedInstanceState) {

    }

    @Override
    protected void initView() {
        boolean isLogin = spUtils.getBoolean(ConstantBasic.STATUS_LOGIN, false);
        if (isLogin) {
            HttpUserEntity userEntity = NormalUtils.getUser(spUtils);
            mPresenter.login(userEntity.getUsername(), userEntity.getPassword());
        } else {
            goToActivity(MainTabActivity.class);
            SplashActivity.this.finish();
        }
    }

    @Override
    public void onResult(int loginOrRegis, HttpUserEntity entity) {
        spUtils.putString(ConstantBasic.ENTITY_USER, new Gson().toJson(entity));
    }

    @Override
    public void onFinish(String type) {
        //自动登录成功
        goToActivity(MainTabActivity.class);
        SplashActivity.this.finish();
    }

    @Override
    public void showMessage(int Type, String message) {

    }

    @Override
    public void onFaild(String type) {
        spUtils.putBoolean(ConstantBasic.STATUS_LOGIN, false);
        spUtils.remove(ConstantBasic.ENTITY_USER);
    }
}
