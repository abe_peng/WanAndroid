package com.abe.basic.android;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.abe.basic.R;
import com.abe.basic.android.fragment.HomeFragment;
import com.abe.basic.android.fragment.SortFragment;
import com.abe.basic.utils.ConstantBasic;
import com.abe.gcore.base.DaggerBaseFActivity;
import com.abe.gcore.base.mvp.IBasePresenter;
import com.abe.gcore.utils.CoreNormalUtils;
import com.abe.gcore.utils.SPUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class MainTabActivity extends DaggerBaseFActivity {

    HomeFragment homeFragment;
    SortFragment sortFragment;
    @BindView(R.id.item_viewpager)
    ViewPager itemViewpager;
    @BindView(R.id.item_image_home)
    ImageView itemImageHome;
    @BindView(R.id.item_image_sort)
    ImageView itemImageSort;
    @BindView(R.id.item_txt_home)
    TextView itemTxtHome;
    @BindView(R.id.item_txt_sort)
    TextView itemTxtSort;

    @Inject
    SPUtils spUtils;

    @Override
    protected IBasePresenter createPresenter() {
        return null;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_main_tab;
    }

    @Override
    protected void init(Bundle savedInstanceState) {

    }

    @Override
    protected void initView() {
        initFragment();
    }

    private void initFragment() {
        homeFragment = HomeFragment.newInstance();
        sortFragment = SortFragment.newInstance();
        itemViewpager
                .setAdapter(new ViewPagerAdapter(getSupportFragmentManager()));
        itemViewpager.addOnPageChangeListener(new PagerListener());
        itemViewpager.setCurrentItem(0);
    }

    @OnClick(R.id.item_home)
    public void onHomeClicked(View view) {
        itemViewpager.setCurrentItem(0);
    }

    @OnClick(R.id.item_sort)
    public void onSortClicked(View view) {
        itemViewpager.setCurrentItem(1);
    }


    @OnClick(R.id.item_title_user)
    public void onUserClicked(View view) {
        //判断登陆
        boolean isLogin = spUtils.getBoolean(ConstantBasic.STATUS_LOGIN, false);
        goToActivity(isLogin ? PersonalActivity.class : LoginActivity.class);
    }

    @OnClick(R.id.item_title_search)
    public void onSearchClicked(View view) {

    }

    private class PagerListener implements ViewPager.OnPageChangeListener {
        @Override
        public void onPageScrollStateChanged(int pos) {
        }

        @Override
        public void onPageScrolled(int pos, float arg1, int arg2) {

        }

        @Override
        public void onPageSelected(int pos) {
            if (pos == 0) {
                itemImageHome.setBackgroundResource(R.drawable.icon_home_h);
                itemImageSort.setBackgroundResource(R.drawable.icon_sort_l);
                itemTxtHome.setTextColor(CoreNormalUtils.getColor(R.color.colorTheme));
                itemTxtSort.setTextColor(CoreNormalUtils.getColor(R.color.txt_5));
            } else {
                itemImageHome.setBackgroundResource(R.drawable.icon_home_l);
                itemImageSort.setBackgroundResource(R.drawable.icon_sort_h);
                itemTxtHome.setTextColor(CoreNormalUtils.getColor(R.color.txt_5));
                itemTxtSort.setTextColor(CoreNormalUtils.getColor(R.color.colorTheme));
            }
        }
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {

        ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int pos) {
            switch (pos) {
                case 0:
                    return homeFragment;
                case 1:
                    return sortFragment;
            }
            throw new IllegalStateException("No fragment at position " + pos);
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}
