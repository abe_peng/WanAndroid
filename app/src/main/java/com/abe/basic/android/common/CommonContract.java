package com.abe.basic.android.common;

import com.abe.basic.model.http.HttpArticleEntity;
import com.abe.basic.model.http.HttpSortEntity;
import com.abe.basic.model.http.HttpUserEntity;
import com.abe.gcore.base.mvp.IView;
import com.abe.basic.model.http.HttpPageEntity;
import com.abe.basic.model.http.HttpBannerEntity;
import com.abe.basic.model.http.HttpFriendLinkEntity;

import java.util.List;

//协议文件
public class CommonContract {

    public interface MainPresenter {
        void getBanner();

        void getArticleByPage(int page);

        void getFriendLink();
    }

    public interface MainView extends IView {
        void onGetBanner(List<HttpBannerEntity> entities);

        void onGetArticles(HttpPageEntity<HttpArticleEntity> entity);

        void onGetFriendLink(List<HttpFriendLinkEntity> entities);
    }

    public interface SortPresenter {
        void getArticleTree();

        void getTreeArticles(int page,int cid);
    }

    public interface SortView extends IView {
        void onGetArticleTree(List<HttpSortEntity> entities);

        void onGetTreeArticles(HttpPageEntity<HttpArticleEntity> entity);
    }

    public interface LoginRegisPresenter {
        void login(String username, String password);

        void register(String username, String password, String passworda);
    }

    public interface LoginRegisView extends IView {
        /**
         * @param loginOrRegis 0为注册，1为登录
         */
        void onResult(int loginOrRegis, HttpUserEntity entity);
    }
}
