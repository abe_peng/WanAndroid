package com.abe.basic.android;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.abe.basic.R;
import com.abe.basic.android.fragment.CollectArticleFragment;
import com.abe.basic.android.fragment.CollectWebFragment;
import com.abe.basic.model.http.HttpUserEntity;
import com.abe.basic.utils.ConstantBasic;
import com.abe.basic.utils.NormalUtils;
import com.abe.gcore.base.DaggerBaseFActivity;
import com.abe.gcore.base.mvp.IBasePresenter;
import com.abe.gcore.utils.CoreNormalUtils;
import com.abe.gcore.utils.SPUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class PersonalActivity extends DaggerBaseFActivity {

    CollectArticleFragment collectArticleFragment;
    CollectWebFragment collectWebFragment;

    @BindView(R.id.item_name)
    TextView itemName;
    @BindView(R.id.item_menu_in_txt)
    TextView itemMenuInTxt;
    @BindView(R.id.item_menu_in_img)
    ImageView itemMenuInImg;
    @BindView(R.id.item_menu_out_txt)
    TextView itemMenuOutTxt;
    @BindView(R.id.item_menu_out_img)
    ImageView itemMenuOutImg;
    @BindView(R.id.item_viewpager)
    ViewPager itemViewpager;

    @Inject
    SPUtils spUtils;

    @Override
    protected IBasePresenter createPresenter() {
        return null;
    }


    @Override
    protected int getLayoutResId() {
        return R.layout.activity_persional;
    }

    @Override
    protected void init(Bundle savedInstanceState) {

    }

    @Override
    protected void initView() {
        HttpUserEntity userEntity = NormalUtils.getUser(spUtils);
        itemName.setText(userEntity.getUsername());
        initFragment();
    }

    private void initFragment() {
        collectArticleFragment = CollectArticleFragment.newInstance();
        collectWebFragment = CollectWebFragment.newInstance();
        itemViewpager
                .setAdapter(new ViewPagerAdapter(getSupportFragmentManager()));
        itemViewpager.addOnPageChangeListener(new PagerListener());
        itemViewpager.setCurrentItem(0);
    }

    @OnClick(R.id.item_back)
    public void onBackClicked(View view) {
        onBackPressed();
    }

    @OnClick(R.id.item_logout)
    public void onLogoutClicked(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this).setTitle("提示").setMessage("退出登录？").setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                spUtils.putBoolean(ConstantBasic.STATUS_LOGIN, false);
                spUtils.remove(ConstantBasic.ENTITY_USER);
                onBackPressed();
            }
        });
        builder.show();
    }

    @OnClick(R.id.item_menu_in_txt)
    public void onMenuInClicked(View view) {
        itemViewpager.setCurrentItem(0);
    }

    @OnClick(R.id.item_menu_out_txt)
    public void onMenuOutClicked(View view) {
        itemViewpager.setCurrentItem(1);
    }

    private class PagerListener implements ViewPager.OnPageChangeListener {
        @Override
        public void onPageScrollStateChanged(int pos) {
        }

        @Override
        public void onPageScrolled(int pos, float arg1, int arg2) {

        }

        @Override
        public void onPageSelected(int pos) {
            int colorTheme = CoreNormalUtils.getColor(R.color.colorTheme);
            int colorNormal = CoreNormalUtils.getColor(R.color.txt_5);
            if (pos == 0) {
                itemMenuInTxt.setTextColor(colorTheme);
                itemMenuInImg.setBackgroundColor(colorTheme);
                itemMenuOutTxt.setTextColor(colorNormal);
                itemMenuOutImg.setBackgroundColor(Color.TRANSPARENT);
            } else {
                itemMenuInTxt.setTextColor(colorNormal);
                itemMenuInImg.setBackgroundColor(Color.TRANSPARENT);
                itemMenuOutTxt.setTextColor(colorTheme);
                itemMenuOutImg.setBackgroundColor(colorTheme);
            }
        }
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {

        ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int pos) {
            switch (pos) {
                case 0:
                    return collectArticleFragment;
                case 1:
                    return collectWebFragment;
            }
            throw new IllegalStateException("No fragment at position " + pos);
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}
