package com.abe.basic.android;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.abe.basic.R;
import com.abe.basic.android.common.CommonContract;
import com.abe.basic.android.common.presenter.LoginRegisPresenter;
import com.abe.basic.model.http.HttpUserEntity;
import com.abe.basic.utils.ConstantBasic;
import com.abe.basic.utils.NormalUtils;
import com.abe.gcore.base.DaggerBaseActivity;
import com.abe.gcore.utils.CoreNormalUtils;
import com.abe.gcore.utils.SPUtils;
import com.google.gson.Gson;
import com.maning.mndialoglibrary.MProgressDialog;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class LoginActivity extends DaggerBaseActivity<CommonContract.LoginRegisView, LoginRegisPresenter> implements CommonContract.LoginRegisView {
    @BindView(R.id.login_txt_name)
    EditText loginTxtName;
    @BindView(R.id.login_txt_password)
    EditText loginTxtPassword;

    @Inject
    SPUtils spUtils;

    MProgressDialog mProgressDialog;

    @Override
    protected LoginRegisPresenter createPresenter() {
        return new LoginRegisPresenter();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_login;
    }

    @Override
    protected void init(Bundle savedInstanceState) {

    }

    @Override
    protected void initView() {
        mProgressDialog = NormalUtils.creatDialog(this);
        boolean isLogin = spUtils.getBoolean(ConstantBasic.STATUS_LOGIN, false);
        if (isLogin) {
            goToActivity(PersonalActivity.class);
        }
    }

    @OnClick(R.id.login_button)
    public void onViewLogin(View view) {
        String username = CoreNormalUtils.getText(loginTxtName);
        String password = CoreNormalUtils.getText(loginTxtPassword);
        if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password)) {
            CoreNormalUtils.showShortToast("请输入用户名密码");
        }
        mPresenter.login(username, password);
        mProgressDialog.show();
    }

    @OnClick(R.id.login_button_register)
    public void onViewRegister(View view) {
        startActivityForResult(new Intent(this,RegisterActivity.class), REQUEST_REGISTER);
    }

    private int REQUEST_REGISTER = 100;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_REGISTER && resultCode == RESULT_OK) {
            onBackPressed();
        }
    }

    @Override
    public void onResult(int loginOrRegis, HttpUserEntity entity) {
        if (loginOrRegis == 1) {
            spUtils.putBoolean(ConstantBasic.STATUS_LOGIN, true);
            spUtils.putString(ConstantBasic.ENTITY_USER, new Gson().toJson(entity));
            onBackPressed();
        }
    }

    @Override
    public void onFinish(String type) {
        mProgressDialog.dismiss();
    }

    @Override
    public void showMessage(int Type, String message) {

    }

    @Override
    public void onFaild(String type) {

    }
}
