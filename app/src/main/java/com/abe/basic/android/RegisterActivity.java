package com.abe.basic.android;


import android.os.Bundle;
import android.text.TextUtils;
import android.widget.EditText;

import com.abe.basic.R;
import com.abe.basic.android.common.CommonContract;
import com.abe.basic.android.common.presenter.LoginRegisPresenter;
import com.abe.basic.model.http.HttpUserEntity;
import com.abe.basic.utils.ConstantBasic;
import com.abe.basic.utils.NormalUtils;
import com.abe.gcore.base.DaggerBaseActivity;
import com.abe.gcore.utils.CoreNormalUtils;
import com.abe.gcore.utils.SPUtils;
import com.google.gson.Gson;
import com.maning.mndialoglibrary.MProgressDialog;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class RegisterActivity extends DaggerBaseActivity<CommonContract.LoginRegisView, LoginRegisPresenter> implements CommonContract.LoginRegisView {
    @BindView(R.id.register_txt_name)
    EditText registerTxtName;
    @BindView(R.id.register_txt_password)
    EditText registerTxtPassword;
    @BindView(R.id.register_txt_password_a)
    EditText registerTxtPasswordA;

    @Inject
    SPUtils SPUtils;

    MProgressDialog mProgressDialog;

    @Override
    protected LoginRegisPresenter createPresenter() {
        return new LoginRegisPresenter();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_register;
    }

    @Override
    protected void init(Bundle savedInstanceState) {

    }

    @Override
    protected void initView() {
        mProgressDialog = NormalUtils.creatDialog(this);
    }

    @OnClick(R.id.register_button)
    public void onViewClicked() {
        String username = CoreNormalUtils.getText(registerTxtName);
        String password = CoreNormalUtils.getText(registerTxtPassword);
        String passwordA = CoreNormalUtils.getText(registerTxtPasswordA);
        if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password) || TextUtils.isEmpty(passwordA)) {
            CoreNormalUtils.showShortToast("请输入用户名密码");
            return;
        }
        if (!password.equals(passwordA)) {
            CoreNormalUtils.showShortToast("两次密码输入不一致");
            return;
        }
        mPresenter.register(username, password, passwordA);
        mProgressDialog.show();
    }

    @Override
    public void onResult(int loginOrRegis, HttpUserEntity entity) {
        mProgressDialog.dismiss();
        if (loginOrRegis == 0) {
            CoreNormalUtils.showShortToast("注册成功");
            mPresenter.login(entity.getUsername(), entity.getPassword());
        } else {
            SPUtils.putBoolean(ConstantBasic.STATUS_LOGIN, true);
            SPUtils.putString(ConstantBasic.ENTITY_USER, new Gson().toJson(entity));
            this.setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    public void onFinish(String type) {
        mProgressDialog.dismiss();
    }

    @Override
    public void showMessage(int Type, String message) {

    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }

    @Override
    public void onFaild(String type) {
        mProgressDialog.dismiss();
    }
}
