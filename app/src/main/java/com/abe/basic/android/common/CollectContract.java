package com.abe.basic.android.common;


import com.abe.basic.model.http.HttpCollectArticleEntity;
import com.abe.basic.model.http.HttpPageEntity;
import com.abe.basic.model.http.HttpWebEntity;
import com.abe.gcore.base.mvp.IView;

import java.util.List;

public class CollectContract {
    public interface CollectArticlePresenter {
        void getCollectList(int page);

        void collectIn(int id);

        void collectOut(String title, String author, String link);

        void unCollectIn(int id);

        void unCollect(int id, int originId);
    }

    public interface CollectArticleView extends IView {
        void onGetCollectArticles(HttpPageEntity<HttpCollectArticleEntity> pageEntity);

        void onRequest(String type, boolean isSucc);
    }

    public interface CollectWebPresenter {
        void getCollectList();

        void collectAdd(String name, String link);

        void collectUpdate(int id, String name, String link);

        void unCollect(int id);
    }

    public interface CollectWebView extends IView {
        void onGetWebList(List<HttpWebEntity> webEntities);

        void onRequest(String type, boolean isSucc);
    }
}
