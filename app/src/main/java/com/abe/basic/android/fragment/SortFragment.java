package com.abe.basic.android.fragment;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.abe.basic.R;
import com.abe.basic.android.SortArticleActivity;
import com.abe.basic.android.common.CommonContract;
import com.abe.basic.android.common.presenter.SortPresenter;
import com.abe.basic.model.http.HttpArticleEntity;
import com.abe.basic.model.http.HttpPageEntity;
import com.abe.basic.model.http.HttpSortEntity;
import com.abe.gcore.base.DaggerBaseFragment;
import com.abe.gcore.utils.CoreNormalUtils;
import com.yanzhenjie.recyclerview.swipe.widget.DefaultItemDecoration;
import com.zhy.adapter.recyclerview.CommonAdapter;
import com.zhy.adapter.recyclerview.MultiItemTypeAdapter;
import com.zhy.adapter.recyclerview.base.ViewHolder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

public class SortFragment extends DaggerBaseFragment<CommonContract.SortView, SortPresenter> implements CommonContract.SortView {

    @BindView(R.id.item_rv)
    RecyclerView itemRv;

    List<HttpSortEntity> sortEntities;
    CommonAdapter<HttpSortEntity> sortAdapter;

    @Override
    protected SortPresenter createPresenter() {
        return new SortPresenter();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.item_view_common_recyclerview;
    }

    @Override
    protected void initDatas() {
        sortEntities = new ArrayList<>();
        sortAdapter = new CommonAdapter<HttpSortEntity>(getContext(), R.layout.item_list_sort_data, sortEntities) {
            @Override
            protected void convert(ViewHolder holder, HttpSortEntity httpSortEntity, int position) {
                holder.setText(R.id.item_super_name, httpSortEntity.getName());
                StringBuilder children = new StringBuilder();
                for (int i = 0; i < httpSortEntity.getChildren().size(); i++) {
                    HttpSortEntity sortEntity = httpSortEntity.getChildren().get(i);
                    if (i != 0) children.append("\t\t");
                    children.append(sortEntity.getName());
                }
                holder.setText(R.id.item_t_sort_name, children.toString());
            }
        };
        initRV();
        mPresenter.getArticleTree();
    }

    private void initRV() {
        itemRv.setLayoutManager(new LinearLayoutManager(getContext()));
        itemRv.addItemDecoration(new DefaultItemDecoration(CoreNormalUtils.getColor(R.color.transparent)));
        itemRv.setAdapter(sortAdapter);
        sortAdapter.setOnItemClickListener(new MultiItemTypeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
                HashMap<String, Object> map = new HashMap<>();
                map.put("sort", sortEntities.get(position));
                goToActivity(SortArticleActivity.class, map);
            }

            @Override
            public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, int position) {
                return false;
            }
        });
    }

    @Override
    public void onGetArticleTree(List<HttpSortEntity> entities) {
        if (entities == null) {
            return;
        }
        sortEntities.clear();
        sortEntities.addAll(entities);
        itemRv.post(new Runnable() {
            @Override
            public void run() {
                sortAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onGetTreeArticles(HttpPageEntity<HttpArticleEntity> entity) {

    }

    @Override
    public void onFinish(String type) {

    }

    @Override
    public void showMessage(int Type, String message) {

    }

    @Override
    public void onFaild(String type) {

    }

    public SortFragment() {
    }

    public static SortFragment newInstance() {
        return new SortFragment();
    }
}
