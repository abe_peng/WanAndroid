package com.abe.basic.android.receiver;

import android.content.Context;
import android.content.Intent;

import com.abe.basic.android.common.CommonContract;
import com.abe.basic.android.common.presenter.LoginRegisPresenter;
import com.abe.basic.model.http.HttpUserEntity;
import com.abe.basic.utils.ConstantBasic;
import com.abe.basic.utils.NormalUtils;
import com.abe.gcore.utils.CoreNormalUtils;
import com.abe.gcore.utils.SPUtils;
import com.google.gson.Gson;

import javax.inject.Inject;

import dagger.android.DaggerBroadcastReceiver;

public class CommonReceiver extends DaggerBroadcastReceiver implements CommonContract.LoginRegisView {

    @Inject
    LoginRegisPresenter loginRegisPresenter;

    @Inject
    SPUtils spUtils;

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        String action = intent.getAction();
        CoreNormalUtils.w(action);
        if (action == null)
            return;
        switch (action) {
            case ConstantBasic.ABE_BASIC_AUTO_LOGIN: {
                HttpUserEntity userEntity = NormalUtils.getUser(spUtils);
                loginRegisPresenter.login(userEntity.getUsername(), userEntity.getPassword());
            }
            break;
        }
    }

    @Override
    public void onResult(int loginOrRegis, HttpUserEntity entity) {
        if (loginOrRegis == 1) {
            //登录成功,更新信息
            spUtils.putString(ConstantBasic.ENTITY_USER, new Gson().toJson(entity));
            //发送登录信息更新广播
            CoreNormalUtils.sendLocalBroadcast(ConstantBasic.ABE_BASIC_USERENTITY_UPDATE);
        }
    }

    @Override
    public void onFinish(String type) {

    }

    @Override
    public void showMessage(int Type, String message) {

    }

    @Override
    public void onFaild(String type) {
        //登录失败
        spUtils.putBoolean(ConstantBasic.STATUS_LOGIN, false);
        spUtils.remove(ConstantBasic.ENTITY_USER);
    }
}
