package com.abe.basic.api.service;

import com.abe.basic.model.http.HttpUserEntity;
import com.abe.basic.utils.ConstantAPI;
import com.abe.gcore.base.BaseTResultEntity;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface CommonService {

    @FormUrlEncoded
    @POST(ConstantAPI.WApiLogin)
    Observable<BaseTResultEntity<HttpUserEntity>> login(@Field("username") String username, @Field("password") String password);

    @FormUrlEncoded
    @POST(ConstantAPI.WApiRegister)
    Observable<BaseTResultEntity<HttpUserEntity>> register(@Field("username") String username, @Field("password") String password, @Field("repassword") String repassword);
}
