package com.abe.basic.api.service;

import com.abe.basic.model.http.HttpArticleEntity;
import com.abe.gcore.base.BaseTResultEntity;
import com.abe.basic.model.http.HttpPageEntity;
import com.abe.basic.model.http.HttpBannerEntity;
import com.abe.basic.model.http.HttpFriendLinkEntity;
import com.abe.basic.utils.ConstantAPI;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface MainService {
    @GET(ConstantAPI.WApiArticle)
    Observable<BaseTResultEntity<HttpPageEntity<HttpArticleEntity>>> getArticleByPage(@Path("page") int page);

    @GET(ConstantAPI.WApiBanner)
    Observable<BaseTResultEntity<List<HttpBannerEntity>>> getBanner();

    @GET(ConstantAPI.WApiFriendLink)
    Observable<BaseTResultEntity<List<HttpFriendLinkEntity>>> getFriendLink();
}
