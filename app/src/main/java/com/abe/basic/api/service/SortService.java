package com.abe.basic.api.service;

import com.abe.basic.model.http.HttpArticleEntity;
import com.abe.basic.model.http.HttpPageEntity;
import com.abe.basic.utils.ConstantAPI;
import com.abe.gcore.base.BaseTResultEntity;
import com.abe.basic.model.http.HttpSortEntity;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface SortService {
    @GET(ConstantAPI.WApiTree)
    Observable<BaseTResultEntity<List<HttpSortEntity>>> getSortData();

    @GET(ConstantAPI.WApiTreeArticle)
    Observable<BaseTResultEntity<HttpPageEntity<HttpArticleEntity>>> getSortArticle(@Path("page") int page, @Query("cid") int cid);
}
