package com.abe.basic.api;

import com.abe.basic.api.service.CollectService;
import com.abe.basic.api.service.CommonService;
import com.abe.basic.api.service.MainService;
import com.abe.gcore.base.http.BaseApi;
import com.abe.basic.api.service.SortService;
import com.abe.basic.utils.ConstantAPI;

public class AppMainService {

    private static CommonService commonService;
    private static MainService mainService;
    private static SortService sortService;
    private static CollectService collectService;

    //重选地址的初始化
    public static void initService() {
        commonService = null;
        mainService = null;
        sortService = null;
    }

    public static CommonService getCommonService() {
        if (commonService == null) {
            commonService = BaseApi.retrofit(ConstantAPI.WApiBase).create(CommonService.class);
        }
        return commonService;
    }

    public static MainService getMainService() {
        if (mainService == null) {
            mainService = BaseApi.retrofit(ConstantAPI.WApiBase).create(MainService.class);
        }
        return mainService;
    }

    public static CollectService getCollectService() {
        if (collectService == null) {
            collectService = BaseApi.retrofit(ConstantAPI.WApiBase).create(CollectService.class);
        }
        return collectService;
    }

    public static SortService getSortService() {
        if (sortService == null) {
            sortService = BaseApi.retrofit(ConstantAPI.WApiBase).create(SortService.class);
        }
        return sortService;
    }
}
