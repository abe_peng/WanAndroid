package com.abe.basic.api.service;

import com.abe.basic.model.http.HttpCollectArticleEntity;
import com.abe.basic.model.http.HttpMsgEntity;
import com.abe.basic.model.http.HttpPageEntity;
import com.abe.basic.model.http.HttpWebEntity;
import com.abe.basic.utils.ConstantAPI;
import com.abe.gcore.base.BaseTResultEntity;


import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface CollectService {
    @GET(ConstantAPI.WApiCollectArticleList)
    Observable<BaseTResultEntity<HttpPageEntity<HttpCollectArticleEntity>>> getArticleList(@Path("page") int page);

    @POST(ConstantAPI.WApiCollectInArticle)
    Observable<HttpMsgEntity> collectInArticle(@Path("id") int id);

    @FormUrlEncoded
    @POST(ConstantAPI.WApiCollectOutArticle)
    Observable<HttpMsgEntity> collectOutArticle(@Field("title") String title, @Field("author") String author, @Field("link") String link);

    @POST(ConstantAPI.WApiUnCollectArticleIn)
    Observable<HttpMsgEntity> unCollectArticleIn(@Path("id") int id);

    @FormUrlEncoded
    @POST(ConstantAPI.WApiUnCollectArticle)
    Observable<HttpMsgEntity> unCollectArticle(@Path("id") int id, @Field("originId") int originId);

    @GET(ConstantAPI.WApiCollectWebList)
    Observable<BaseTResultEntity<List<HttpWebEntity>>> getWebList();

    @FormUrlEncoded
    @POST(ConstantAPI.WApiCollectAddWeb)
    Observable<HttpMsgEntity> addCollectWeb(@Field("name") String name, @Field("link") String link);

    @FormUrlEncoded
    @POST(ConstantAPI.WApiCollectUpdateWeb)
    Observable<HttpMsgEntity> updateCollectWeb(@Field("id") int id, @Field("name") String name, @Field("link") String link);

    @FormUrlEncoded
    @POST(ConstantAPI.WApiUnCollectWeb)
    Observable<HttpMsgEntity> unCollectWeb(@Field("id") int id);
}
