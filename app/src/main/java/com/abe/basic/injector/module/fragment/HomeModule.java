package com.abe.basic.injector.module.fragment;

import com.abe.basic.android.common.presenter.CollectArticlePresenter;
import com.abe.basic.android.common.presenter.LoginRegisPresenter;
import com.abe.basic.android.fragment.HomeFragment;

import dagger.Module;
import dagger.Provides;

@Module
public abstract class HomeModule {

    @Provides
    static CollectArticlePresenter providersCollectArticlePresenter(HomeFragment homeFragment) {
        CollectArticlePresenter presenter = new CollectArticlePresenter();
        presenter.attachView(homeFragment);
        return presenter;
    }

    @Provides
    static String provideName() {
        return HomeFragment.class.getName();
    }

}
