package com.abe.basic.injector.component;

import android.content.Context;

import com.abe.basic.injector.module.AllActivitysModule;
import com.abe.basic.injector.module.AllFragmentsModule;
import com.abe.basic.injector.module.AllReceiversModule;
import com.abe.gcore.base.BaseAbeApplication;
import com.abe.gcore.base.inject.moudle.AppModule;
import com.abe.gcore.utils.SPUtils;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {AndroidInjectionModule.class,
        AndroidSupportInjectionModule.class,
        AppModule.class,
        AllActivitysModule.class,
        AllFragmentsModule.class, AllReceiversModule.class})
public interface AppComponent {
    void inject(BaseAbeApplication application);

    BaseAbeApplication application();

    Context context();

    SPUtils SPUtils();
}