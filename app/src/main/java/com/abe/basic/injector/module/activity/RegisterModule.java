package com.abe.basic.injector.module.activity;

import com.abe.basic.android.RegisterActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class RegisterModule {
    @Provides
    static String provideName() {
        return RegisterActivity.class.getName();
    }
}
