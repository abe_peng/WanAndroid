package com.abe.basic.injector.module.fragment;

import com.abe.basic.android.fragment.SortFragment;

import dagger.Module;
import dagger.Provides;

@Module
public abstract class SortModule {

    @Provides
    static String provideName() {
        return SortFragment.class.getName();
    }
}
