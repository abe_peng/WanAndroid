package com.abe.basic.injector.module.fragment;


import com.abe.basic.android.fragment.CollectWebFragment;

import dagger.Module;
import dagger.Provides;


@Module
public abstract class CollectWebModule {
    @Provides
    static String provideName() {
        return CollectWebFragment.class.getName();
    }
}
