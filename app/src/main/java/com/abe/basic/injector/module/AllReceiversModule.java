package com.abe.basic.injector.module;

import com.abe.basic.android.receiver.CommonReceiver;
import com.abe.basic.injector.module.receiver.CommonBRModule;
import com.abe.gcore.base.inject.component.BroadcastsComponent;
import com.abe.gcore.base.inject.scope.ActivityScope;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module(subcomponents = {
        BroadcastsComponent.class
})
public abstract class AllReceiversModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = CommonBRModule.class)
    abstract CommonReceiver contributeCommonReceiverInjector();
}
