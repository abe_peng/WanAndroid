package com.abe.basic.injector.module;

import com.abe.basic.android.fragment.CollectArticleFragment;
import com.abe.basic.android.fragment.CollectWebFragment;
import com.abe.basic.android.fragment.HomeFragment;
import com.abe.basic.android.fragment.SortArticleFragment;
import com.abe.basic.android.fragment.SortFragment;
import com.abe.basic.injector.module.fragment.CollectArticleModule;
import com.abe.basic.injector.module.fragment.CollectWebModule;
import com.abe.basic.injector.module.fragment.HomeModule;
import com.abe.basic.injector.module.fragment.SortArticleModule;
import com.abe.basic.injector.module.fragment.SortModule;
import com.abe.gcore.base.inject.component.BaseFragmentComponent;
import com.abe.gcore.base.inject.scope.FragmentScope;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module(subcomponents = {
        BaseFragmentComponent.class
})
public abstract class AllFragmentsModule {

    @FragmentScope
    @ContributesAndroidInjector(modules = HomeModule.class)
    abstract HomeFragment contributeHomeFargmentInjector();

    @FragmentScope
    @ContributesAndroidInjector(modules = SortModule.class)
    abstract SortFragment contributeSortFargmentInjector();

    @FragmentScope
    @ContributesAndroidInjector(modules = CollectArticleModule.class)
    abstract CollectArticleFragment contributeCollectArticleFragmentInjector();

    @FragmentScope
    @ContributesAndroidInjector(modules = CollectWebModule.class)
    abstract CollectWebFragment contributeCollectWebFragmentInjector();

    @FragmentScope
    @ContributesAndroidInjector(modules = SortArticleModule.class)
    abstract SortArticleFragment contributeSortArticleFragmentInjector();
}

