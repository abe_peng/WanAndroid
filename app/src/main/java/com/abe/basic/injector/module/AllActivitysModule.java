package com.abe.basic.injector.module;

import com.abe.basic.android.CommonWebActivity;
import com.abe.basic.android.LoginActivity;
import com.abe.basic.android.MainTabActivity;
import com.abe.basic.android.PersonalActivity;
import com.abe.basic.android.RegisterActivity;
import com.abe.basic.android.SortArticleActivity;
import com.abe.basic.android.SplashActivity;
import com.abe.basic.injector.module.activity.LoginModule;
import com.abe.basic.injector.module.activity.MainModule;
import com.abe.basic.injector.module.activity.PersonalModule;
import com.abe.basic.injector.module.activity.RegisterModule;
import com.abe.basic.injector.module.activity.SplashModule;
import com.abe.basic.injector.module.activity.WebModule;
import com.abe.basic.injector.module.fragment.SortModule;
import com.abe.gcore.base.inject.component.BaseActivityComponent;
import com.abe.gcore.base.inject.component.BaseFActivityComponent;
import com.abe.gcore.base.inject.scope.ActivityScope;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module(subcomponents = {
        BaseActivityComponent.class, BaseFActivityComponent.class
})
public abstract class AllActivitysModule {
    @ActivityScope
    @ContributesAndroidInjector(modules = SplashModule.class)
    abstract SplashActivity contributeSplashActivityInjector();

    @ActivityScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract MainTabActivity contributeMainTabActivityInjector();

    @ActivityScope
    @ContributesAndroidInjector(modules = LoginModule.class)
    abstract LoginActivity contributeLoginActivityInjector();

    @ActivityScope
    @ContributesAndroidInjector(modules = RegisterModule.class)
    abstract RegisterActivity contributeRegisterActivityInjector();

    @ActivityScope
    @ContributesAndroidInjector(modules = WebModule.class)
    abstract CommonWebActivity contributeCommonWebActivityInjector();

    @ActivityScope
    @ContributesAndroidInjector(modules = PersonalModule.class)
    abstract PersonalActivity contributePersonalActivityInjector();

    @ActivityScope
    @ContributesAndroidInjector(modules = SortModule.class)
    abstract SortArticleActivity contributeSortArticleActivityInjector();
}
