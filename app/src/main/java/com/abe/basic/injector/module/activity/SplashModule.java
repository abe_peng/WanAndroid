package com.abe.basic.injector.module.activity;


import com.abe.basic.android.SplashActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class SplashModule {
    @Provides
    static String provideName() {
        return SplashActivity.class.getName();
    }
}