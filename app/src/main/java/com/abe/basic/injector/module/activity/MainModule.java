package com.abe.basic.injector.module.activity;

import com.abe.basic.android.MainTabActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class MainModule {
    @Provides
    static String provideName() {
        return MainTabActivity.class.getName();
    }
}
