package com.abe.basic.injector.module.fragment;

import com.abe.basic.android.common.presenter.CollectArticlePresenter;
import com.abe.basic.android.fragment.HomeFragment;
import com.abe.basic.android.fragment.SortArticleFragment;

import dagger.Module;
import dagger.Provides;

@Module
public abstract class SortArticleModule {
    @Provides
    static CollectArticlePresenter providersCollectArticlePresenter(SortArticleFragment fragment) {
        CollectArticlePresenter presenter = new CollectArticlePresenter();
        presenter.attachView(fragment);
        return presenter;
    }
    @Provides
    static String provideName() {
        return SortArticleFragment.class.getName();
    }
}
