package com.abe.basic.injector.module.activity;

import com.abe.basic.android.PersonalActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class PersonalModule {
    @Provides
    static String provideName() {
        return PersonalActivity.class.getName();
    }
}
