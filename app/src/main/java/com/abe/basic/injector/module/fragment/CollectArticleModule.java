package com.abe.basic.injector.module.fragment;


import com.abe.basic.android.fragment.CollectArticleFragment;

import dagger.Module;
import dagger.Provides;


@Module
public abstract class CollectArticleModule {
    @Provides
    static String provideName() {
        return CollectArticleFragment.class.getName();
    }
}
