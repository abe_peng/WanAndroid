package com.abe.basic.injector.module.activity;

import com.abe.basic.android.CommonWebActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class WebModule {
    @Provides
    static String provideName() {
        return CommonWebActivity.class.getName();
    }
}
