package com.abe.basic.injector.module.activity;

import com.abe.basic.android.LoginActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class LoginModule {
    @Provides
    static String provideName() {
        return LoginActivity.class.getName();
    }
}
