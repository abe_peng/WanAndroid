package com.abe.basic.injector.module.receiver;

import com.abe.basic.android.common.presenter.LoginRegisPresenter;
import com.abe.basic.android.receiver.CommonReceiver;

import dagger.Module;
import dagger.Provides;

@Module
public abstract class CommonBRModule {

    @Provides
    static String provideName() {
        return CommonReceiver.class.getName();
    }

    @Provides
    static LoginRegisPresenter provideLoginRegisterPresenter(CommonReceiver receiver) {
        LoginRegisPresenter loginRegisPresenter = new LoginRegisPresenter();
        loginRegisPresenter.attachView(receiver);
        return loginRegisPresenter;
    }
}
