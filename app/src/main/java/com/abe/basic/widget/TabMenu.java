package com.abe.basic.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.abe.basic.R;
import com.abe.gcore.libs.widgets.BaseCustomView;
import com.abe.gcore.utils.ConvertUtils;
import com.abe.gcore.utils.CoreNormalUtils;


public class TabMenu extends BaseCustomView {
    public TabMenu(Context context) {
        this(context, null);
    }

    public TabMenu(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TabMenu(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @Override
    protected int getRootViewId() {
        return R.id.base_layout;
    }

    private TextView menuName;
    private ImageView menuUnderline;
    private int defaultColor;
    private int themeColor;

    private void init() {
        // 使用布局资源填充视图
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // 加载布局文件
        customRootView = (ViewGroup) mInflater.inflate(
                R.layout.view_tab_menu, this, true);
        menuName = customRootView.findViewById(R.id.item_name);
        menuUnderline = customRootView.findViewById(R.id.item_underline);
        defaultColor = context.getResources().getColor(R.color.txt_3);
        themeColor = context.getResources().getColor(R.color.colorTheme);
    }

    public void setDefaultColor(int color) {
        this.defaultColor = color;
    }

    public void setMenuColor(int color) {
        this.themeColor = color;
    }

    public void setMenuName(String name) {
        if (menuName != null) {
            menuName.setText(name);
            float size = menuName.getTextSize();//px
            int height = ConvertUtils.sp2px(27);
            height = height + (int) size;
            //measure方法的参数值都设为0即可
            menuName.measure(0, 0);
            //获取组件宽度
            int customWidth = menuName.getMeasuredWidth();
            setLayoutParams(new ViewGroup.LayoutParams(customWidth, height));
        }
    }

    public void setMenuTextSize(int unit, float size) {
        if (menuName != null) {
            menuName.setTextSize(unit, size);
            setWH(unit, size);
        }
    }

    private void setWH(int unit, float size) {
        if (menuName != null) {
            int height = ConvertUtils.sp2px(27);
            switch (unit) {
                case TypedValue.COMPLEX_UNIT_DIP: {
                    height = height + ConvertUtils.dp2px(size);
                }
                break;
                case TypedValue.COMPLEX_UNIT_SP: {
                    height = height + ConvertUtils.sp2px(size);
                }
                break;
                default: {
                    height = height + (int) size;
                }
                break;
            }
            //measure方法的参数值都设为0即可
            menuName.measure(0, 0);
            //获取组件宽度
            int customWidth = menuName.getMeasuredWidth();
            setLayoutParams(new ViewGroup.LayoutParams(customWidth, height));
        }
    }

    public void setSelector(boolean isSelector) {
        if (menuName != null) {
            menuName.setTextColor(isSelector ? themeColor : defaultColor);
        }
        if (menuUnderline != null) {
            menuUnderline.setBackgroundColor(isSelector ? themeColor : context.getResources().getColor(R.color.transparent));
        }
    }


    @Override
    public void show() {
        //禁掉显示隐藏控制
    }

    @Override
    public void dismiss() {
        //禁掉显示隐藏控制
    }
}
