package com.abe.basic.base;

import com.abe.basic.injector.component.DaggerAppComponent;
import com.abe.gcore.base.BaseAbeApplication;
import com.abe.gcore.utils.CoreNormalUtils;
import com.tencent.smtt.sdk.QbSdk;

public class ManagerAPP extends BaseAbeApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        //搜集本地tbs内核信息并上报服务器，服务器返回结果决定使用哪个内核。

        QbSdk.PreInitCallback cb = new QbSdk.PreInitCallback() {

            @Override
            public void onViewInitFinished(boolean arg0) {
                //x5內核初始化完成的回调，为true表示x5内核加载成功，否则表示x5内核加载失败，会自动切换到系统内核。
                CoreNormalUtils.w("app onViewInitFinished is " + arg0);
            }

            @Override
            public void onCoreInitFinished() {
            }
        };
        //x5内核初始化接口
        QbSdk.initX5Environment(getApplicationContext(), cb);
    }

    @Override
    protected void initInject() {
        //dagger
        DaggerAppComponent.builder().appModule(getAppMoudle()).build().inject(this);
    }
}
