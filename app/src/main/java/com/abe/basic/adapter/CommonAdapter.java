package com.abe.basic.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.abe.basic.R;
import com.abe.basic.model.http.HttpArticleEntity;
import com.abe.gcore.libs.adapter.BaseXApapter;
import com.abe.gcore.libs.adapter.CommonData;
import com.abe.gcore.libs.adapter.autobase.ViewHolderUtils;
import com.abe.gcore.utils.EmptyUtils;

import java.util.List;

public class CommonAdapter extends BaseXApapter {

    public static final String TYPE_HOME_ARTICLE = "TYPE_HOME_ARTICLE";

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CommonData temData = data.get(position);
        switch (temData.getType()) {
            case TYPE_HOME_ARTICLE: {
                convertView = inflater.inflate(R.layout.item_list_home_article, parent, false);
                ViewHolderUtils.ViewHolder viewHolder = vh.get(convertView);
                HttpArticleEntity entity = (HttpArticleEntity) temData.getData();
                if (EmptyUtils.isNotEmpty(entity)) {
                    viewHolder.getTextView(R.id.item_name).setText(entity.getTitle());
                }
            }
            break;
        }
        return convertView;
    }

    public CommonAdapter(Context context, List<CommonData> data) {
        super(context, data);
    }
}
