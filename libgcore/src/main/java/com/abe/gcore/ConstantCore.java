package com.abe.gcore;

import android.os.Environment;

import java.io.File;

public class ConstantCore {
    private static final String SD_PATH = Environment.getExternalStorageDirectory().getAbsolutePath();
    private static final String FILE_ROOT_PATH = SD_PATH + File.separator + "abeframe";
    static final String CRASH_PATH = FILE_ROOT_PATH + File.separator + "crash";
    public static final String VIDEO_PATH = FILE_ROOT_PATH + File.separator + "video";
    public static final String IMAGE_PATH = FILE_ROOT_PATH + File.separator + "image";
    public static final String CACHE_PATH = FILE_ROOT_PATH + File.separator + "cache";
    public static final String LOG_PATH = FILE_ROOT_PATH + File.separator + "log";
    public static final String APP_SP_BASE = "com.abe.gcore";

    //MVP 访问失败
    public static final String MVP_PRESENTER_ERROR_ViewAttached = "mvp_presenter_error_not_viewattached";
    public static final String MVP_PRESENTER_ERROR_DATA_NULL = "mvp_presenter_error_data_null";

    //本地广播
    public static final String CORE_TOAST = "com.abe.gcore.toast";//toast
    public static final String CORE_APP_BECAME_FOREGROUND = "com.abe.gcore.appfg";//切回前台
    public static final String CORE_APP_BECAME_BACKGROUND = "com.abe.gcore.appbg";//切回后台

    //app前台后台状态改变
    public static final String CORE_APP_BF_STATUS_CHANGE = "CORE_APP_BF_STATUS_CHANGE";
}
