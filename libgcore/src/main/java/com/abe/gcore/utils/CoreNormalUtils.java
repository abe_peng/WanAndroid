package com.abe.gcore.utils;


import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.EditText;
import android.widget.Toast;

import com.abe.gcore.ConstantCore;

public class CoreNormalUtils {
    public static void sendBroadcast(String action) {
        Intent callIntent = new Intent();
        callIntent.setAction(action);
        Utils.getContext().sendBroadcast(callIntent);
    }

    public static void sendLocalBroadcast(String action) {
        Intent callIntent = new Intent();
        callIntent.setAction(action);
        LocalBroadcastManager.getInstance(Utils.getContext()).sendBroadcast(callIntent);
    }

    public static void w(String message) {
        System.out.println(message);
    }

    public static int getColor(int colorRes) {
        return Utils.getContext().getResources().getColor(colorRes);
    }

    public static void showShortToastR(String message) {
        Intent callIntent = new Intent();
        callIntent.putExtra("key", message);
        callIntent.setAction(ConstantCore.CORE_TOAST);
        Utils.getContext().sendBroadcast(callIntent);
    }

    public static void showShortToast(String message) {
        Toast.makeText(Utils.getContext(), message, Toast.LENGTH_SHORT).show();
    }

    public static void showShortToast(int message) {
        Toast.makeText(Utils.getContext(), message, Toast.LENGTH_SHORT).show();
    }

    public static String getText(EditText editText) {
        if (editText == null) {
            showShortToast("文本框Null");
            return "";
        }
        return editText.getText().toString().trim();
    }
}
