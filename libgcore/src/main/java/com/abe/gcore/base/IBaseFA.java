package com.abe.gcore.base;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;

import java.util.HashMap;

import butterknife.ButterKnife;

public abstract class IBaseFA extends FragmentActivity {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResId());
        ButterKnife.bind(this);
        init(savedInstanceState);
        initDatas();
    }

    //获取context
    public Context getThisContext() {
        return this;
    }

    //返回布局资源ID
    protected abstract int getLayoutResId();

    //初始化
    protected abstract void init(Bundle savedInstanceState);

    //实现功能，填充数据
    protected abstract void initDatas();

    //处理传值
    public HashMap<String, Object> getFilter() {
        if (getIntent() != null && getIntent().getSerializableExtra("filter") != null) {
            HashMap<String, Object> filter = (HashMap<String, Object>) getIntent().getSerializableExtra("filter");
            return filter;
        }
        return null;
    }

    /**
     * 跳转到指定页面
     */
    public void goToActivity(Class<?> cls) {
        goToActivity(cls, null);
    }

    /**
     * 跳转到指定页面
     */
    public void goToActivity(Class<?> cls, HashMap<String, Object> filter) {
        Intent intent = new Intent();
        intent.setClass(this, cls);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (filter != null) {
            intent.putExtra("filter", filter);
        }
        startActivity(intent);
        //  overridePendingTransition(R.anim.modal_activity_open_enter, R.anim.modal_activity_open_exit);
    }

    /**
     * 跳转到指定页面
     */
    public void goToActivityForResult(Class<?> cls, int code) {
        goToActivityForResult(cls, code, null);
    }

    /**
     * 跳转到指定页面
     */
    public void goToActivityForResult(Class<?> cls, int code,
                                      HashMap<String, Object> filter) {
        Intent intent = new Intent();
        intent.setClass(this, cls);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (filter != null) {
            intent.putExtra("filter", filter);
        }
        startActivityForResult(intent, code);
    }
    /**
     * 关闭制定页面
     */
    public void CloseActivityForResult(HashMap<String, Object> mode) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("filter", mode);
        Intent intent = new Intent();
        intent.putExtras(bundle);
        // 返回intent
        setResult(RESULT_OK, intent);
        finish();
    }
}
