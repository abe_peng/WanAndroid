package com.abe.gcore.base;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.abe.gcore.base.mvp.IBasePresenter;
import com.abe.gcore.base.mvp.IView;
import com.abe.gcore.base.mvp.ui.MvpBaseActivity;

import dagger.android.AndroidInjection;

public abstract class DaggerBaseActivity<V extends IView, P extends IBasePresenter<V>> extends MvpBaseActivity<V,P> {
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
    }
}
