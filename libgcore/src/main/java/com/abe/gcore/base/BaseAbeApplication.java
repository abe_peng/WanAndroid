package com.abe.gcore.base;

import android.app.Activity;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentProvider;
import android.support.multidex.MultiDexApplication;
import android.support.v4.app.Fragment;

import com.abe.gcore.ConstantCore;
import com.abe.gcore.CrashHandler;
import com.abe.gcore.base.inject.moudle.AppModule;
import com.abe.gcore.utils.CoreNormalUtils;
import com.abe.gcore.utils.Utils;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import dagger.android.HasBroadcastReceiverInjector;
import dagger.android.HasContentProviderInjector;
import dagger.android.HasFragmentInjector;
import dagger.android.HasServiceInjector;
import dagger.android.support.HasSupportFragmentInjector;

public abstract class BaseAbeApplication extends MultiDexApplication implements ForegroundCallbacks.Listener, HasActivityInjector, HasSupportFragmentInjector,
        HasBroadcastReceiverInjector,
        HasFragmentInjector,
        HasServiceInjector,
        HasContentProviderInjector {
    @Inject
    DispatchingAndroidInjector<Activity> dispatchingActivityInjector;
    @Inject
    DispatchingAndroidInjector<Fragment> childSupportFragmentInjector;
    @Inject
    DispatchingAndroidInjector<android.app.Fragment> childFragmentInjector;
    @Inject
    DispatchingAndroidInjector<BroadcastReceiver> broadcastReceiverInjector;
    @Inject
    DispatchingAndroidInjector<Service> serviceInjector;
    @Inject
    DispatchingAndroidInjector<ContentProvider> contentProviderInjector;

    @Override
    public void onCreate() {
        super.onCreate();
        //基础工具
        Utils.init(this);
        //crash
        CrashHandler crashHandler = CrashHandler.getInstance();
        crashHandler.init(this);
        //监听APP前台后台状态
        ForegroundCallbacks foregroundCallbacks = ForegroundCallbacks.init(this);
        foregroundCallbacks.addListener(this);
        //dagger
        initInject();
    }

    protected abstract void initInject();

    protected AppModule getAppMoudle() {
        return new AppModule(this);
    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return dispatchingActivityInjector;
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return childSupportFragmentInjector;
    }

    @Override
    public AndroidInjector<android.app.Fragment> fragmentInjector() {
        return childFragmentInjector;
    }

    @Override
    public AndroidInjector<BroadcastReceiver> broadcastReceiverInjector() {
        return broadcastReceiverInjector;
    }

    @Override
    public AndroidInjector<ContentProvider> contentProviderInjector() {
        return contentProviderInjector;
    }

    @Override
    public AndroidInjector<Service> serviceInjector() {
        return serviceInjector;
    }

    @Override
    public void onBecameForeground() {
        CoreNormalUtils.w("app---Foreground");
        //切换回前台广播
        CoreNormalUtils.sendBroadcast(ConstantCore.CORE_APP_BECAME_FOREGROUND);
    }

    @Override
    public void onBecameBackground() {
        CoreNormalUtils.w("app---Background");
        //切换回前台广播
        CoreNormalUtils.sendBroadcast(ConstantCore.CORE_APP_BECAME_BACKGROUND);
    }
}