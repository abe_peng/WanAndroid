package com.abe.gcore.base.inject.moudle;

import android.content.Context;

import com.abe.gcore.ConstantCore;
import com.abe.gcore.base.BaseAbeApplication;
import com.abe.gcore.utils.SPUtils;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private final BaseAbeApplication application;

    public AppModule(BaseAbeApplication application) {
        this.application = application;
    }

    @Provides
    @Singleton
    BaseAbeApplication provideApplication() {
        return application;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return application;
    }

    @Provides
    @Singleton
    SPUtils provideSPUtils() {
        return new SPUtils(ConstantCore.APP_SP_BASE);
    }
}
