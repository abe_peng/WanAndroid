package com.abe.gcore.base.mvp.ui;

import com.abe.gcore.base.IBaseF;
import com.abe.gcore.base.mvp.IBasePresenter;
import com.abe.gcore.base.mvp.IView;


public abstract class MvpBaseFragment<V extends IView, P extends IBasePresenter<V>> extends IBaseF {

    protected P mPresenter;

    protected abstract P createPresenter();

    @Override
    public void init() {
        //MVP
        mPresenter = createPresenter();
        if (mPresenter != null) {
            mPresenter.attachView((V) this);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mPresenter != null) {
            mPresenter.attachView((V) this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mPresenter != null) {
            mPresenter.detachView();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mPresenter != null) {
            mPresenter.detachView();
        }
    }
}
