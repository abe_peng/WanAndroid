package com.abe.gcore.base.mvp.ui;

import com.abe.gcore.base.IBaseFA;
import com.abe.gcore.base.mvp.IBasePresenter;
import com.abe.gcore.base.mvp.IView;

public abstract class MvpBaseFragmentActivity<V extends IView, P extends IBasePresenter<V>> extends IBaseFA {

    protected P mPresenter;

    protected abstract P createPresenter();

    protected abstract void initView();

    @Override
    public void initDatas() {
        mPresenter = createPresenter();//创建Presenter
        //填充View
        if (mPresenter != null) {
            mPresenter.attachView((V) this);
        }
        initView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mPresenter != null) {
            mPresenter.detachView();
        }
    }
}
