package com.abe.gcore.base;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.abe.gcore.ConstantCore;
import com.abe.gcore.utils.CoreNormalUtils;

public class BaseAbeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        CoreNormalUtils.w(action);
        if (action == null)
            return;
        switch (action) {
            case ConstantCore.CORE_TOAST: {
                String msg = intent.getStringExtra("key");
                CoreNormalUtils.showShortToast(msg);
            }
        }
    }

}
