package com.abe.gcore.base.inject.component;

import com.abe.gcore.base.DaggerBaseFragment;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Subcomponent(modules = {
        AndroidSupportInjectionModule.class,
})
public interface BaseFragmentComponent extends AndroidInjector<DaggerBaseFragment> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<DaggerBaseFragment> {
    }
}
