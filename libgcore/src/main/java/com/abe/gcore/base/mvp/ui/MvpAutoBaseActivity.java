package com.abe.gcore.base.mvp.ui;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.View;

import com.abe.gcore.base.mvp.IBasePresenter;
import com.abe.gcore.base.mvp.IView;
import com.zhy.autolayout.AutoFrameLayout;
import com.zhy.autolayout.AutoLinearLayout;
import com.zhy.autolayout.AutoRelativeLayout;

import java.util.HashMap;

public abstract class MvpAutoBaseActivity<V extends IView, P extends IBasePresenter<V>> extends MvpBaseActivity<V, P> {

    private static final String LAYOUT_LINEARLAYOUT = "LinearLayout";
    private static final String LAYOUT_FRAMELAYOUT = "FrameLayout";
    private static final String LAYOUT_RELATIVELAYOUT = "RelativeLayout";


    @Override
    public View onCreateView(String name, Context context, AttributeSet attrs) {
        View view = null;
        if (name.equals(LAYOUT_FRAMELAYOUT)) {
            view = new AutoFrameLayout(context, attrs);
        }

        if (name.equals(LAYOUT_LINEARLAYOUT)) {
            view = new AutoLinearLayout(context, attrs);
        }

        if (name.equals(LAYOUT_RELATIVELAYOUT)) {
            view = new AutoRelativeLayout(context, attrs);
        }

        if (view != null) return view;

        return super.onCreateView(name, context, attrs);
    }

    /**
     * 跳转到指定页面
     *
     * @param cls
     * @param code
     */
    public void goToActivityForResult(Class<?> cls, int code) {
        goToActivityForResult(cls, code, null);
    }

    /**
     * 跳转到指定页面
     *
     * @param cls
     * @param code
     * @param filter
     */
    public void goToActivityForResult(Class<?> cls, int code,
                                      HashMap<String, Object> filter) {
        Intent intent = new Intent();
        intent.setClass(this, cls);
        if (filter != null) {
            intent.putExtra("filter", filter);
        }
        startActivityForResult(intent, code);
    }
}
