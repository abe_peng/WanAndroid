package com.abe.gcore.base.mvp;

public interface IView {
    void onFinish(String type);

    void showMessage(int Type, String message);

    void onFaild(String type);
}
