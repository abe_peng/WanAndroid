package com.abe.gcore.base.http;

import com.abe.gcore.utils.klog.KLog;
import com.google.gson.Gson;

import java.io.IOException;
import java.net.URLDecoder;
import java.nio.charset.Charset;

import okhttp3.Connection;
import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;
import okio.BufferedSource;

class BaseOKHttpInterceptor implements Interceptor {

    private static final Charset UTF8 = Charset.forName("UTF-8");
    public static final MediaType MEDIA_TYPE
            = MediaType.parse("application/json; charset=utf-8");

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        // 获得Connection，内部有route、socket、handshake、protocol方法
        Connection connection = chain.connection();
        // 如果Connection为null，返回HTTP_1_1，否则返回connection.protocol()
        Protocol protocol = connection != null ? connection.protocol() : Protocol.HTTP_1_1;
        // 比如: --> POST http://121.40.227.8:8088/api http/1.1
        String requestStartMessage = "--> " + request.method() + ' ' + request.url() + ' ' + protocol;
        KLog.json("requestStartMessage = " + requestStartMessage);
        // 打印 Response
        Response response;
        try {
            response = chain.proceed(request);
        } catch (Exception e) {
            KLog.json("Exception = " + new Gson().toJson(e));
            throw e;
        }
        ResponseBody responseBody = response.body();
        long contentLength = responseBody.contentLength();
        String bodyValue = "NONE";
        BufferedSource source = responseBody.source();
        source.request(Long.MAX_VALUE); // Buffer the entire body.
        Buffer buffer = source.buffer();
        if (contentLength != 0) {
            // 获取Response的body的字符串 并打印
            bodyValue = URLDecoder.decode(buffer.clone().readString(UTF8), "UTF-8");
        }
        //bodyValue = formatString(bodyValue);
        KLog.json("response = " + bodyValue);
        return response;
    }

    /**
     * 处理带反斜杠的json数据
     *
     * @param result 待处理数据
     */
    private String formatString(String result) {
        if (result.length() < 2) return result;
        String testStr = result.substring(0, 1);
        if (testStr.equals("{")||testStr.equals("[")) return result;
        String backlogJsonStrTmp = result.replace("\\", "");
        // 处理完成后赋值回去
        result = backlogJsonStrTmp.substring(1, backlogJsonStrTmp.length() - 1);
        return result;
    }

    private boolean bodyEncoded(Headers headers) {
        String contentEncoding = headers.get("Content-Encoding");
        return contentEncoding != null && !contentEncoding.equalsIgnoreCase("identity");
    }
}