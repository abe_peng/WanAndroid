package com.abe.gcore.base.mvp.ui;

import com.abe.gcore.base.IBaseA;
import com.abe.gcore.base.mvp.IBasePresenter;
import com.abe.gcore.base.mvp.IView;

public abstract class MvpBaseActivity<V extends IView, P extends IBasePresenter<V>> extends IBaseA {

    protected P mPresenter;

    protected abstract P createPresenter();

    protected abstract void initView();

    @Override
    protected void initDatas() {
        //创建Presenter
        mPresenter = createPresenter();
        //填充View
        if (mPresenter != null) {
            mPresenter.attachView((V) this);
        }
        initView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mPresenter != null) {
            mPresenter.detachView();
        }
    }
}
