package com.abe.gcore.base.mvp.rxandroid;


import io.reactivex.disposables.Disposable;

public interface IRxPresenter {
    /**
     * 将Disposable添加
     *
     * @param subscription subscription
     */
    void addDisposable(Disposable subscription);

    /**
     * 在界面退出等需要解绑观察者的情况下调用此方法统一解绑，防止Rx造成的内存泄漏
     */
    void unDisposable();
}
