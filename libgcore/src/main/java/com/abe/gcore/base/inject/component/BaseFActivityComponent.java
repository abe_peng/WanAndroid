package com.abe.gcore.base.inject.component;


import com.abe.gcore.base.DaggerBaseFActivity;

import dagger.Subcomponent;
import dagger.android.AndroidInjectionModule;
import dagger.android.AndroidInjector;

@Subcomponent(modules = {
        AndroidInjectionModule.class,
})
public interface BaseFActivityComponent extends AndroidInjector<DaggerBaseFActivity> {

    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<DaggerBaseFActivity> {
    }

}
