package com.abe.gcore.base.inject.component;


import com.abe.gcore.base.DaggerBaseActivity;

import dagger.Subcomponent;
import dagger.android.AndroidInjectionModule;
import dagger.android.AndroidInjector;

@Subcomponent(modules = {
        AndroidInjectionModule.class,
})
public interface BaseActivityComponent extends AndroidInjector<DaggerBaseActivity> {

    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<DaggerBaseActivity> {
    }

}
