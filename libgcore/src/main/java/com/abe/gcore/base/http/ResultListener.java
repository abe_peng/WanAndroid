package com.abe.gcore.base.http;

public interface ResultListener<T> {

    public void onStart();
    public void onEnd();
    public void onSuccess(T data);
    public void onFailure(String message);
}
