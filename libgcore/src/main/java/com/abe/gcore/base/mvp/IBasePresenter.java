package com.abe.gcore.base.mvp;

import com.abe.gcore.ConstantCore;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;

public abstract class IBasePresenter<V extends IView> implements IPresenter<V> {
    private Reference<V> mViewRef;//View 接口类型的弱引用

    public void attachView(V view) {
        mViewRef = new WeakReference<V>(view);
    }

    public void detachView() {
        if (mViewRef != null) {
            mViewRef.clear();
            mViewRef = null;
        }
    }

    public V getView() {
        return mViewRef.get();
    }

    public boolean isViewAttached() {
        return mViewRef != null && mViewRef.get() != null;
    }

    protected void showViewAttachedError() {
        w(this.getClass().getName() + ":" + ConstantCore.MVP_PRESENTER_ERROR_ViewAttached);
    }

    protected void showViewDataNullError() {
        w(this.getClass().getSimpleName() + ":" + ConstantCore.MVP_PRESENTER_ERROR_DATA_NULL);
    }

    //控制台输出
    protected static void w(String message) {
        System.out.println(message);
    }
}
