package com.abe.gcore.base.http;

public class BaseApiException extends Exception {
    public BaseApiException(String msg)
    {
        super(msg);
    }
}
