package com.abe.gcore.base.http.cookies;


import android.content.Context;

import com.abe.gcore.base.http.cookies.PersistentCookieStore;

import java.util.List;

import io.reactivex.annotations.NonNull;
import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;

public class CookieManager implements CookieJar {

    private Context mContext;
    private static PersistentCookieStore COOKIE_STORE;

    public CookieManager(Context context) {
        mContext = context;
        if (COOKIE_STORE == null) {
            COOKIE_STORE = new PersistentCookieStore(mContext);
        }
    }

    @Override
    public void saveFromResponse(@NonNull HttpUrl url, @NonNull List<Cookie> cookies) {
        if (cookies.size() > 0) {
            for (Cookie item : cookies) {
                COOKIE_STORE.add(url, item);
            }
        }
    }

    @Override
    public List<Cookie> loadForRequest(@NonNull HttpUrl url) {
        return COOKIE_STORE.get(url);
    }

    /**
     * 清除所有cookie
     */
    public static void clearAllCookies() {
        COOKIE_STORE.removeAll();
    }

    /**
     * 清除指定cookie
     *
     * @param url    HttpUrl
     * @param cookie Cookie
     * @return if clear cookies
     */
    public static boolean clearCookies(HttpUrl url, Cookie cookie) {
        return COOKIE_STORE.remove(url, cookie);
    }

    /**
     * 获取cookies
     *
     * @return List<Cookie>
     */
    public static List<Cookie> getCookies() {
        return COOKIE_STORE.getCookies();
    }

}
