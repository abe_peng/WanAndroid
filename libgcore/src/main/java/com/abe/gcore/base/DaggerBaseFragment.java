package com.abe.gcore.base;


import android.app.Activity;

import com.abe.gcore.base.mvp.IBasePresenter;
import com.abe.gcore.base.mvp.IView;
import com.abe.gcore.base.mvp.ui.MvpBaseFragment;

import dagger.android.support.AndroidSupportInjection;

public abstract class DaggerBaseFragment<V extends IView, P extends IBasePresenter<V>> extends MvpBaseFragment<V, P> {
    @Override
    public void onAttach(Activity activity) {
        AndroidSupportInjection.inject(this);
        super.onAttach(activity);
    }
}
