package com.abe.gcore.base.mvp.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.abe.gcore.base.mvp.IBasePresenter;
import com.abe.gcore.base.mvp.IView;
import com.zhy.autolayout.utils.AutoUtils;

public abstract class MvpAutoBaseFragment<V extends IView, P extends IBasePresenter<V>> extends MvpBaseFragment<V, P> {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        AutoUtils.autoSize(view);
        return view;
    }
}
