package com.abe.gcore.libs.adapter;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.widget.BaseAdapter;


import com.abe.gcore.libs.adapter.autobase.ViewHolderUtils;

import java.util.List;

public abstract class BaseXApapter extends BaseAdapter {
    protected LayoutInflater inflater;
    protected List<CommonData> data;
    protected Context context;
    protected Float scale;

    /**
     * 这是一个生产 ViewHolder的工具类
     */
    protected ViewHolderUtils vh;

    public BaseXApapter(Context context, List<CommonData> data) {
        this.data = data;
        this.context = context;
        this.vh = new ViewHolderUtils();
        this.scale = getScale(context);
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return data != null ? data.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return data != null ? data.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private float getScale(Context context) {
        DisplayMetrics displayMetrics = context.getResources()
                .getDisplayMetrics();
        float width = displayMetrics.widthPixels;
        return width / 1280;
    }
}
