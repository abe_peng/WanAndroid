package com.abe.gcore.libs.clickeffect;

import android.annotation.SuppressLint;
import android.view.MotionEvent;
import android.view.View;


@SuppressLint("ClickableViewAccessibility")
public abstract class OnLongClickEffectTouchListener implements View.OnTouchListener {

    public abstract void onTouchStart();

    public abstract void onTouchMove();

    public abstract void onTouchCancel();

    public abstract void onTouchEnd();

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mViewClickEffect.onPressedEffect(v);
                v.setPressed(true);
                onTouchStart();
                break;
            case MotionEvent.ACTION_MOVE:
                float x = event.getX();
                float y = event.getY();
                boolean isInside = (x > 0 && x < v.getWidth() && y > 0 && y < v
                        .getHeight());
                if (v.isPressed() != isInside) {
                    v.setPressed(isInside);
                }
                onTouchMove();
                break;
            case MotionEvent.ACTION_CANCEL:
                mViewClickEffect.onUnPressedEffect(v);
                v.setPressed(false);
                onTouchCancel();
                break;
            case MotionEvent.ACTION_UP:
                mViewClickEffect.onUnPressedEffect(v);
                if (v.isPressed()) {
                    v.performClick();
                    v.setPressed(false);
                }
                onTouchEnd();
                break;
        }
        return true;
    }

    /**
     * View的点击状态效果定义
     */
    private ViewClickEffect mViewClickEffect = new DefaultClickEffectScaleAnimate();

    public void setViewClickEffect(ViewClickEffect viewClickEffect) {
        mViewClickEffect = viewClickEffect;
    }
}
