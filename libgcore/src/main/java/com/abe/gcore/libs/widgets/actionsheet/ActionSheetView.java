package com.abe.gcore.libs.widgets.actionsheet;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.abe.gcore.R;
import com.abe.gcore.libs.widgets.BaseCustomView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ActionSheetView extends BaseCustomView implements AdapterView.OnItemClickListener,
        View.OnClickListener {

    public interface OnActionSheetClickListener {
        void OnCancel();

        void OnSelect(int pos, String menu);
    }

    public ActionSheetView(Context context) {
        this(context, null);
    }

    public ActionSheetView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ActionSheetView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private LinearLayout baseView;
    private List<String> buttonNames;
    private ActionSheetAdapter adapter;
    private OnActionSheetClickListener listener;

    public void setListener(OnActionSheetClickListener listener) {
        this.listener = listener;
    }

    @Override
    protected int getRootViewId() {
        return R.id.base_layout;
    }

    private void init() {
        // 使用布局资源填充视图
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // 加载布局文件
        customRootView = (ViewGroup) mInflater.inflate(
                R.layout.view_action_sheet, this, true);
        baseView = customRootView.findViewById(R.id.as_base_layout);
        ListView as_button_list = customRootView.findViewById(R.id.as_button_list);
        Button as_button_cancel = customRootView.findViewById(R.id.as_button_cancel);
        baseView.setOnClickListener(this);
        as_button_cancel.setOnClickListener(this);
        as_button_list.setOnItemClickListener(this);
        buttonNames = new ArrayList<>();
        adapter = new ActionSheetAdapter();
        as_button_list.setAdapter(adapter);
    }

    /**
     * 设置菜单
     *
     * @param menus    菜单列表数据
     * @param listener 点击监听
     */
    public void setMenuItem(String[] menus, OnActionSheetClickListener listener) {
        List<String> list = Arrays.asList(menus);
        setMenuItem(list, listener);
    }

    /**
     * 设置菜单
     *
     * @param menus    菜单列表数据
     * @param listener 点击监听
     */
    public void setMenuItem(List<String> menus, OnActionSheetClickListener listener) {
        if (menus == null) return;
        this.buttonNames = menus;
        this.listener = listener;
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        if (listener == null) return;
        listener.OnCancel();
        dismiss();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        if (listener == null) return;
        listener.OnSelect(i, buttonNames.get(i));
        dismiss();
    }

    private class ActionSheetAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return buttonNames == null ? 0 : buttonNames.size();
        }

        @Override
        public Object getItem(int i) {
            return buttonNames == null ? null : buttonNames.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHolder holder;
            if (view == null) {
                view = LayoutInflater.from(context).inflate(R.layout.item_list_action_sheet, viewGroup, false);
                holder = new ViewHolder(view);
                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }
            holder.button.setText(buttonNames == null ? "" : buttonNames.get(i));
            return view;
        }
    }

    private class ViewHolder {
        private TextView button;

        ViewHolder(View view) {
            button = view.findViewById(R.id.item_button);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        LinearLayout.LayoutParams params = (LayoutParams) baseView
                .getLayoutParams();
        params.height = getScreenHeight(context) - getStatusHeight(context)
                - dip2px(context, buttonNames == null ? 0 : buttonNames.size() * 45 + 70);
        baseView.setLayoutParams(params);
    }
}
