package com.abe.gcore.libs.widgets;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

/**
 * 自定义View基本框架
 * 功能：view显示隐藏
 */
public abstract class BaseCustomView extends LinearLayout {
    protected Context context;
    /**
     * 自定义view根节点
     */
    protected ViewGroup customRootView;
    /**
     * activity的根View
     */
    protected FrameLayout decorView;

    /**
     * 配置自定义view的根view的ID
     */
    protected abstract int getRootViewId();

    public BaseCustomView(Context context) {
        this(context, null);
    }

    public BaseCustomView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BaseCustomView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        this.decorView = getParentContent((Activity) context);
    }

    /**
     * 展示
     */
    public void show() {
        if (isShowing()) {
            return;
        }
        decorView.addView(customRootView);
    }

    /**
     * 隐藏view
     */
    public void dismiss() {
        if (!isShowing()) {
            return;
        }
        decorView.removeView(customRootView);
    }

    /**
     * 检测该View是不是已经添加到根视图
     *
     * @return 如果视图已经存在该View返回true
     */
    public boolean isShowing() {
        View view = decorView.findViewById(getRootViewId());
        return (view != null);
    }

    private static FrameLayout getParentContent(Activity activity) {
        View decorView = activity.getWindow().getDecorView();
        return (FrameLayout) decorView.findViewById(android.R.id.content);
    }

    /**
     * 获得状态栏的高度
     *
     * @param context 上下文
     */
    protected int getStatusHeight(Context context) {
        int statusHeight = -1;
        try {
            Class<?> clazz = Class.forName("com.android.internal.R$dimen");
            Object object = clazz.newInstance();
            int height = Integer.parseInt(clazz.getField("status_bar_height")
                    .get(object).toString());
            statusHeight = context.getResources().getDimensionPixelSize(height);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return statusHeight;
    }

    /**
     * 获得屏幕宽度
     *
     * @param context 上下文
     */
    protected int getScreenWeight(Context context) {
        WindowManager wm = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics outMetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(outMetrics);
        return outMetrics.widthPixels;
    }

    /**
     * 获得屏幕高度
     *
     * @param context 上下文
     */
    protected int getScreenHeight(Context context) {
        WindowManager wm = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics outMetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(outMetrics);
        return outMetrics.heightPixels;
    }

    /**
     * 根据手机的分辨率从 px(像素) 的单位 转成为 dp
     */

    protected int px2dip(Context context, float pxValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }

    /**
     * 根据手机的分辨率从 dp 的单位 转成为 px(像素)
     */

    protected int dip2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }
}
