package com.abe.gcore.libs.adapter;

public class CommonData {
	private String type;
	private Object data;

	public CommonData() {

	}

	public CommonData(String type, Object data) {
		setData(data);
		setType(type);
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
}
