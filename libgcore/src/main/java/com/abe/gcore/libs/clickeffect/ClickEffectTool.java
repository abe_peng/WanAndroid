package com.abe.gcore.libs.clickeffect;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.lang.reflect.Field;

/**
 * 重新布局工具类, 迭代的形式将视图层级按比例缩放；
 *
 * @author {YueJinbiao}
 */
public class ClickEffectTool {

    /**
     * 统一添加点击效果；
     *
     * @param view 单个视图，或视图层级
     */
    public static void unifiedAddClickEffect(View view) {

        if (view == null) {
            return;
        }

        addClickEffect(view);

        if (view instanceof ViewGroup) {
            View[] children = null;
            try {
                Field field = ViewGroup.class.getDeclaredField("mChildren");
                field.setAccessible(true);
                children = (View[]) field.get(view);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
            if (children != null) {
                for (View child : children) {
                    unifiedAddClickEffect(child);
                }
            }
        }
    }

    /**
     * @param view 不考虑嵌套， 为单个View添加点击效果；
     */
    public static void addClickEffect(View view) {
        if (view instanceof Button) {
            view.setOnTouchListener(new OnClickEffectTouchListener());
        }
    }
}
