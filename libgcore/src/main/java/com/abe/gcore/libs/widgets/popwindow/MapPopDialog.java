package com.abe.gcore.libs.widgets.popwindow;

import android.content.Context;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsoluteLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.abe.gcore.R;
import com.abe.gcore.libs.widgets.BaseCustomView;
import com.abe.gcore.utils.ConvertUtils;

//地图悬浮窗体
public class MapPopDialog extends BaseCustomView implements View.OnClickListener {
    public MapPopDialog(Context context) {
        this(context, null);
    }

    public MapPopDialog(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MapPopDialog(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public interface PopClickListener {
        void close();
    }

    @Override
    protected int getRootViewId() {
        return R.id.base_layout;
    }

    private LinearLayout absRootView;
    private LinearLayout baseRootView;
    private View customView;
    private LinearLayout item_close;
    private TextView item_name;
    private float customHeight;
    private float customWidth;
    private int topXHeight;//依赖区域上边界距离屏幕顶部高度
    private int fixedHeight = 0;//固定高度
    private int fixedWidth = 0;//固定高度

    public void setListener(PopClickListener listener) {
        this.listener = listener;
    }

    private PopClickListener listener;

    private void init() {
        fixedHeight = ConvertUtils.dp2px(60);
        fixedWidth = ConvertUtils.dp2px(20);
        // 使用布局资源填充视图
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // 加载布局文件
        customRootView = (ViewGroup) mInflater.inflate(
                R.layout.view_pop_window, this, true);
        item_close = customRootView.findViewById(R.id.item_close);
        item_name = customRootView.findViewById(R.id.item_name);
        absRootView = customRootView.findViewById(R.id.abs_root_view);
        baseRootView = customRootView.findViewById(R.id.item_custom_root);
        item_close.setOnClickListener(this);
        setNoClose(true);
    }

    //设置标题
    public void setTitle(String title) {
        item_name.setText(title);
    }

    //关闭按钮隐藏
    public void setNoClose(boolean isNot) {
        if (isNot) {
            fixedHeight = ConvertUtils.dp2px(20);
            item_close.setVisibility(View.GONE);
        } else {
            fixedHeight = ConvertUtils.dp2px(60);
            item_close.setVisibility(View.VISIBLE);
        }
    }

    //设置自定义区域
    public void setCustomView(View view, int topX) {
        baseRootView.removeAllViews();
        topXHeight = topX;
        customView = view;
        baseRootView.addView(view);
    }

    //设置显示位置
    public void showPoint(Point point) {
        updatePopPoint(point);
        show();
    }

    @Override
    public void onClick(View view) {
        dismiss();
        if (listener != null) {
            listener.close();
        }
    }

    /**
     * 实时更新弹出气泡窗口的位置
     */
    public void updatePopPoint(Point point) {
        if (customView != null) {
            //measure方法的参数值都设为0即可
            customView.measure(0, 0);
            //获取组件宽度
            customWidth = customView.getMeasuredWidth() + fixedWidth;
            //获取组件高度
            customHeight = customView.getMeasuredHeight() + fixedHeight;
        }
        AbsoluteLayout.LayoutParams geoLP = new AbsoluteLayout.LayoutParams((int) customWidth, (int) customHeight, point.x
                - (int) customWidth / 2, point.y - (int) customHeight + topXHeight);
        absRootView.setLayoutParams(geoLP);
        absRootView.invalidate();
        decorView.requestLayout();
    }
}
